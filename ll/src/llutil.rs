use crate::*;
use core::fmt;

fn map_format_join<T>(vec: Vec<T>, formatter: Box<dyn Fn(T) -> String>, join: &str) -> String {
    vec.into_iter()
        .map(formatter)
        .collect::<Vec<String>>()
        .join(join)
}

impl std::fmt::Display for Uid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::fmt::Display for Gid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::fmt::Display for Tid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::fmt::Display for Lbl {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::fmt::Display for Ty {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Ty::Void => write!(f, "void"),
            Ty::I1 => write!(f, "i1"),
            Ty::I8 => write!(f, "i8"),
            Ty::I64 => write!(f, "i64"),
            Ty::Ptr(ty) => write!(f, "{}*", *ty),
            Ty::Struct(ts) => write!(
                f,
                "{{ {} }}",
                ts.iter()
                    .map(|t| format!("{}", *t))
                    .collect::<Vec<String>>()
                    .join(", ")
            ),
            Ty::Array(i64, ty) => write!(f, "[{i64} x {}]", *ty),
            Ty::Fun(args, ty) => write!(
                f,
                "{} ({})",
                *ty,
                args.iter()
                    .map(|t| format!("{}", *t))
                    .collect::<Vec<String>>()
                    .join(", ")
            ),
            Ty::Namedt(tid) => write!(f, "%{tid}"),
        }
    }
}

fn deref_ptr(ty: Ty) -> Ty {
    match ty {
        Ty::Ptr(p) => *p,
        _ => panic!("PP: expected pointer type"),
    }
}

impl std::fmt::Display for Operand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Operand::Null => write!(f, "null"),
            Operand::Const(i) => write!(f, "{i}"),
            Operand::Gid(g) => write!(f, "@{g}"),
            Operand::Id(u) => write!(f, "%{u}"),
        }
    }
}

fn string_of_operand_pointer((t, v): (Ty, Operand)) -> String {
    format!("{t} {v}")
}

impl std::fmt::Display for Bop {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Bop::Add => write!(f, "add"),
            Bop::Sub => write!(f, "sub"),
            Bop::Mul => write!(f, "mul"),
            Bop::Shl => write!(f, "shl"),
            Bop::Lshr => write!(f, "lshr"),
            Bop::Ashr => write!(f, "ashr"),
            Bop::And => write!(f, "and"),
            Bop::Or => write!(f, "or"),
            Bop::Xor => write!(f, "xor"),
        }
    }
}

impl std::fmt::Display for Cnd {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Cnd::Eq => write!(f, "eq"),
            Cnd::Ne => write!(f, "ne"),
            Cnd::Slt => write!(f, "slt"),
            Cnd::Sle => write!(f, "sle"),
            Cnd::Sgt => write!(f, "sgt"),
            Cnd::Sge => write!(f, "sge"),
        }
    }
}

fn string_of_gep_index(op: Operand) -> String {
    match op {
        Operand::Const(i) => format!("i32 {i}"),
        o => format!("i64 {o}"),
    }
}

impl std::fmt::Display for Insn {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Insn::Binop(b, t, o1, o2) => write!(f, "{b} {t} {o1}, {o2}"),
            Insn::Alloca(t) => write!(f, "alloca {t}"),
            Insn::Load(t, o) => {
                write!(f, "load {}, {t} {o}", deref_ptr(t.clone()))
            }
            Insn::Store(t, os, od) => {
                write!(f, "store {t} {os}, {} {od}", Ty::Ptr(Box::from(t.clone())),)
            }
            Insn::Icmp(c, t, o1, o2) => write!(f, "icmp {c} {t} {o1}, {o2}"),
            Insn::Call(t, o, oa) => write!(
                f,
                "call {t} {o}({})",
                map_format_join(oa.clone(), Box::new(string_of_operand_pointer), ", ")
            ),
            Insn::Bitcast(t1, o, t2) => write!(f, "bitcast {t1} {o} to {t2}"),
            Insn::Gep(t, o, oi) => write!(
                f,
                "getelementptr {}, {t} {o}, {}",
                deref_ptr(t.clone()),
                map_format_join(oi.clone(), Box::new(string_of_gep_index), ", ")
            ),
        }
    }
}

fn string_of_named_insn((u, i): (Uid, Insn)) -> String {
    match i {
        Insn::Store(_, _, _) | Insn::Call(Ty::Void, _, _) => format!("{i}"),
        _ => format!("%{u} = {i}"),
    }
}

impl std::fmt::Display for Terminator {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Terminator::Ret(_, None) => write!(f, "ret void"),
            Terminator::Br(l) => write!(f, "br label %{l}"),
            Terminator::Ret(t, Some(o)) => {
                write!(f, "ret {t} {o}")
            }
            Terminator::Cbr(o, l, m) => {
                write!(f, "br i1 {o}, label %{l}, label %{m}")
            }
        }
    }
}

impl std::fmt::Display for Cfg {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}\n{}",
            self.0,
            self.1
                .clone()
                .into_iter()
                .map(|(l, b)| format!("\n{l}:\n{b}"))
                .collect::<Vec<String>>()
                .join("\n")
        )
    }
}

impl std::fmt::Display for Block {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}\n\t{}",
            self.insns
                .clone()
                .into_iter()
                .map(|ins| format!("\t{}", string_of_named_insn(ins)))
                .collect::<Vec<String>>()
                .join("\n"),
            self.term.1,
        )
    }
}

fn string_of_named_fdecl((g, f): (Gid, Fdecl)) -> String {
    let string_of_arg = |(t, u)| format!("{t} %{u}");
    let (ts, t) = f.f_ty;
    format!(
        "define {t} @{g}({}) {{\n{}\n}}\n",
        ts.into_iter()
            .zip(f.f_param.into_iter())
            .into_iter()
            .map(string_of_arg)
            .collect::<Vec<String>>()
            .join(", "),
        f.f_cfg
    )
}

impl std::fmt::Display for Gdecl {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", self.0, self.1)
    }
}

impl std::fmt::Display for Ginit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Ginit::GNull => write!(f, "null"),
            Ginit::GGid(g) => write!(f, "@{g}"),
            Ginit::GInt(i) => write!(f, "{i}"),
            Ginit::GString(s) => write!(f, "c\"{s}\\00\""),
            Ginit::GArray(gis) => write!(
                f,
                "[ {} ]",
                gis.iter()
                    .map(|ins| format!(" {}", ins))
                    .collect::<Vec<String>>()
                    .join(", "),
            ),
            Ginit::GStruct(gis) => write!(
                f,
                "{{ {} }}",
                gis.iter()
                    .map(|ins| format!(" {}", ins))
                    .collect::<Vec<String>>()
                    .join(", "),
            ),
            Ginit::GBitcast(t1, g, t2) => write!(f, "bitcast ({} {} to {})", t1, *g, t2),
        }
    }
}

fn string_of_named_gdecl((g, gd): (Gid, Gdecl)) -> String {
    format!("@{g} = global {gd}")
}

fn string_of_named_tdecl((n, t): (Tid, Ty)) -> String {
    format!("%{n} = type {t}")
}

fn string_of_named_edecl((g, t): (Gid, Ty)) -> String {
    match t {
        Ty::Fun(ts, rt) => format!(
            "declare {rt} @{g}({})",
            ts.into_iter()
                .map(|ins| format!("{}", ins))
                .collect::<Vec<String>>()
                .join(", "),
        ),
        _ => format!("@{g} = external global {t}"),
    }
}

impl std::fmt::Display for Prog {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}\n\n{}\n\n{}\n\n{}",
            self.tdecls
                .clone()
                .into_iter()
                .map(string_of_named_tdecl)
                .collect::<Vec<String>>()
                .join("\n"),
            self.gdecls
                .clone()
                .into_iter()
                .map(string_of_named_gdecl)
                .collect::<Vec<String>>()
                .join("\n"),
            self.fdecls
                .clone()
                .into_iter()
                .map(string_of_named_fdecl)
                .collect::<Vec<String>>()
                .join("\n"),
            self.edecls
                .clone()
                .into_iter()
                .map(string_of_named_edecl)
                .collect::<Vec<String>>()
                .join("\n"),
        )
    }
}
