use core::panic;
use std::collections::HashMap;

// ll ir compilation --------------------------------------------------------
use crate::platform;
use x86::{self, ind_reg, ins, lbl, lit, reg};

use crate::*;

// Overview ----------------------------------------------------------------- *)

// We suggest that you spend some time understanding this entire file and
// how it fits with the compiler pipeline before making changes.  The suggested
// plan for implementing the compiler is provided on the project web page.

// helpers ------------------------------------------------------------------ *)

// Map LL comparison operations to X86 condition codes
fn compile_cnd(cnd: Cnd) -> x86::Cnd {
    match cnd {
        Cnd::Eq => x86::Cnd::Eq,
        Cnd::Ne => x86::Cnd::Neq,
        Cnd::Slt => x86::Cnd::Lt,
        Cnd::Sle => x86::Cnd::Le,
        Cnd::Sgt => x86::Cnd::Gt,
        Cnd::Sge => x86::Cnd::Ge,
    }
}

fn compile_binop(cnd: Bop) -> x86::Opcode {
    match cnd {
        Bop::Add => x86::Opcode::Addq,
        Bop::Sub => x86::Opcode::Subq,
        Bop::Mul => x86::Opcode::Imulq,
        Bop::Shl => x86::Opcode::Shlq,
        Bop::Lshr => x86::Opcode::Shrq,
        Bop::Ashr => x86::Opcode::Sarq,
        Bop::And => x86::Opcode::Andq,
        Bop::Or => x86::Opcode::Orq,
        Bop::Xor => x86::Opcode::Xorq,
    }
}

// This helper function computes the location of the nth incoming
// function argument: either in a register or relative to %rbp,
// according to the calling conventions.  You might find it useful for
// compile_fdecl.

// [ NOTE: the first six arguments are numbered 0 .. 5 ]

fn arg_loc(n: i64) -> x86::Operand {
    match n {
        0 => x86::Operand::Reg(x86::Reg::Rdi),
        1 => x86::Operand::Reg(x86::Reg::Rsi),
        2 => x86::Operand::Reg(x86::Reg::Rdx),
        3 => x86::Operand::Reg(x86::Reg::Rcx),
        4 => x86::Operand::Reg(x86::Reg::R08),
        5 => x86::Operand::Reg(x86::Reg::R09),
        x => x86::Operand::Ind3(x86::Imm::Lit((x - 4) * 8), x86::Reg::Rbp),
    }
}

// locals and layout --------------------------------------------------------

// One key problem in compiling the LLVM IR is how to map its local
//  identifiers to X86 abstractions.  For the best performance, one
//  would want to use an X86 register for each LLVM %uid.  However,
//  since there are an unlimited number of %uids and only 16 registers,
//  doing so effectively is quite difficult.  We will see later in the
//  course how _register allocation_ algorithms can do a good job at
//  this.

//  A simpler, but less performant, implementation is to map each %uid
//  in the LLVM source to a _stack slot_ (i.e. a region of memory in
//  the stack).  Since LLVMlite, unlike real LLVM, permits %uid locals
//  to store only 64-bit data, each stack slot is an 8-byte value.

//  [ NOTE: For compiling LLVMlite, even i1 data values should be
//  represented as a 8-byte quad. This greatly simplifies code
//  generation. ]

//  We call the datastructure that maps each %uid to its stack slot a
//  'stack layout'.  A stack layout maps a uid to an X86 operand for
//  accessing its contents.  For this compilation strategy, the operand
//  is always an offset from %rbp (in bytes) that represents a storage slot in
//  the stack.

type Layout = HashMap<Uid, x86::Operand>;

// A context contains the global type declarations (needed for getelementptr
//     calculations) and a stack layout.

#[derive(Debug, Clone)]
struct Ctxt {
    pub tdecls: HashMap<Tid, Ty>,
    pub layout: Layout,
}

// compiling operands  ------------------------------------------------------

// LLVM IR instructions support several kinds of operands.

//   LL local %uids live in stack slots, whereas global ids live at
//   global addresses that must be computed from a label.  Constants are
//   immediately available, and the operand Null is the 64-bit 0 value.

//      NOTE: two important facts about global identifiers:

//      (1) You should use (Platform.mangle gid) to obtain a string
//      suitable for naming a global label on your platform (OS X expects
//      "_main" while linux expects "main").

//      (2) 64-bit assembly labels are not allowed as immediate operands.
//      That is, the X86 code: movq _gid %rax which looks like it should
//      put the address denoted by _gid into %rax is not allowed.
//      Instead, you need to compute an %rip-relative address using the
//      leaq instruction:   leaq _gid(%rip).

//   One strategy for compiling instruction operands is to use a
//   designated register (or registers) for holding the values being
//   manipulated by the LLVM IR instruction. You might find it useful to
//   implement the following helper function, whose job is to generate
//   the X86 instruction that moves an LLVM operand into a designated
//   destination (usually a register).

fn compile_operand(ctxt: Ctxt, dest: x86::Operand) -> Box<dyn FnOnce(Operand) -> x86::Ins> {
    Box::new(move |op| match op {
        Operand::Null => ins!(Movq, lit!(0), dest),
        Operand::Const(x) => ins!(Movq, lit!(x), dest),
        Operand::Gid(g) => ins!(
            Leaq,
            x86::Operand::Ind3(x86::Imm::Lbl(platform::mangle(&g.0)), x86::Reg::Rip),
            dest
        ),
        Operand::Id(u) => ins!(
            Movq,
            ctxt.layout.get(&u).expect("could not find key").clone(),
            dest.clone()
        ),
    })
}

// compiling call  ----------------------------------------------------------

// You will probably find it helpful to implement a helper function that
// generates code for the LLVM IR call instruction.

// The code you generate should follow the x64 System V AMD64 ABI
// calling conventions, which places the first six 64-bit (or smaller)
// values in registers and pushes the rest onto the stack.  Note that,
// since all LLVM IR operands are 64-bit values, the first six
// operands will always be placed in registers.  (See the notes about
// compiling fdecl below.)

// [ NOTE: It is the caller's responsibility to clean up arguments
// pushed onto the stack, so you must free the stack space after the
// call returns. ]

// [ NOTE: Don't forget to preserve caller-save registers (only if
// needed). ]

fn compile_call(ctxt: Ctxt, fun: &str, uid: Uid, params: Vec<(Ty, Operand)>) -> Vec<x86::Ins> {
    let additional_ps: i64 = (params.len() as i64) - 6;
    let stack_decrease: Vec<x86::Ins> = if additional_ps > 0 {
        vec![ins!(Addq, lit!(8 * additional_ps), reg!(Rsp))]
    } else {
        vec![]
    };
    let mut copy_params: Vec<x86::Ins> = params
        .into_iter()
        .enumerate()
        .flat_map(|(i, (_, llop))| {
            if i < 6 {
                vec![(compile_operand(ctxt.clone(), arg_loc(i.try_into().unwrap()))(llop))]
            } else {
                vec![
                    (compile_operand(ctxt.clone(), x86::Operand::Reg(x86::Reg::R10))(llop)),
                    ins!(x86::Opcode::Pushq, reg!(R10)),
                ]
            }
        })
        .collect();
    copy_params.extend(
        vec![
            ins!(Callq, lbl!(platform::mangle(fun))),
            ins!(
                Movq,
                reg!(Rax),
                ctxt.layout
                    .get(&uid)
                    .expect("could not find uid in layout")
                    .to_owned()
            ),
        ]
        .into_iter(),
    );
    copy_params.extend(stack_decrease);
    copy_params
}

// compiling getelementptr (gep)  -------------------------------------------

// The getelementptr instruction computes an address by indexing into
//  a datastructure, following a path of offsets.  It computes the
//  address based on the size of the data, which is dictated by the
//  data's type.

//  To compile getelementptr, you must generate x86 code that performs
//  the appropriate arithmetic calculations.

// [size_ty] maps an LLVMlite type to a size in bytes.
//   (needed for getelementptr)

//   - the size of a struct is the sum of the sizes of each component
//   - the size of an array of t's with n elements is n * the size of t
//   - all pointers, I1, and I64 are 8 bytes
//   - the size of a named type is the size of its definition

//   - Void, i8, and functions have undefined sizes according to LLVMlite.
//      Your function should simply return 0 in those cases

pub fn size_ty(tdecls: HashMap<Tid, Ty>, t: Ty) -> usize {
    use Ty::*;
    match t {
        Void | I8 | Fun(_, _) => 0,
        I1 | I64 | Ptr(_) => 8,
        Struct(v) => match v.as_slice() {
            [] => panic!("can't create empty struct"),
            [h] => size_ty(tdecls, (*h).as_ref().to_owned()),
            [h, tl @ ..] => {
                size_ty(tdecls.clone(), (*h).as_ref().to_owned())
                    + size_ty(tdecls, Struct(tl.to_owned()))
            }
        },
        Array(size, typ) => (size as usize) * size_ty(tdecls, *typ),
        Namedt(name) => size_ty(
            tdecls.clone(),
            tdecls
                .get(&name)
                .expect("Could not find requested struct")
                .to_owned(),
        ),
    }
}

//  Generates code that computes a pointer value.

//   1. op must be of pointer type: t*
//   2. the value of op is the base address of the calculation
//   3. the first index in the path is treated as the index into an array
//      of elements of type t located at the base address
//   4. subsequent indices are interpreted according to the type t:
//       - if t is a struct, the index must be a constant n and it
//         picks out the n'th element of the struct. [ NOTE: the offset
//         within the struct of the n'th element is determined by the
//         sizes of the types of the previous elements ]
//       - if t is an array, the index can be any operand, and its
//         value determines the offset within the array.
//       - if t is any other type, the path is invalid
//   5. if the index is valid, the remainder of the path is computed as
//      in (4), but relative to the type f the sub-element picked out
//      by the path so far

fn compile_gep(ctxt: Ctxt, op: (Ty, Operand), path: Vec<Operand>) -> Vec<x86::Ins> {
    let base_addr =
        vec![(compile_operand(ctxt.clone(), x86::Operand::Reg(x86::Reg::R08))(op.clone().1))];
    fn gep2(ctxt: Ctxt, t: Ty, path: Vec<Operand>, acc: Vec<x86::Ins>) -> Vec<x86::Ins> {
        match (t, path.as_slice()) {
            (_, []) => acc,
            (Ty::I64, _) => panic!("I64"),
            (Ty::I8, _) => panic!("I8"),
            (Ty::I1, _) => panic!("I1"),
            (Ty::Void, _) => panic!("Void"),
            (Ty::Fun(_, _), _) => panic!("Fun"),
            (Ty::Ptr(_), _) => panic!("Ptr"),
            (Ty::Namedt(t), _) => gep2(
                ctxt.clone(),
                ctxt.tdecls
                    .get(&t)
                    .expect("Could not find struct")
                    .to_owned(),
                path,
                acc,
            ),
            (Ty::Struct(t_list), [Operand::Const(x), tl @ ..]) => {
                let (struct_size, _) = t_list.clone().into_iter().fold((0, 0), |(sum, ct), t| {
                    (
                        sum + size_ty(ctxt.tdecls.clone(), *t) * ((ct < *x) as usize),
                        ct + 1,
                    )
                });
                let mut acc_cpy = acc.to_vec();
                acc_cpy.extend(vec![ins!(Addq, lit!(struct_size as i64), reg!(R08))]);
                gep2(
                    ctxt,
                    (*t_list[x.to_owned() as usize]).clone(),
                    tl.to_vec(),
                    acc_cpy,
                )
            }
            (Ty::Array(_, t), [h, tl @ ..]) => {
                let calculate_offset = vec![
                    (compile_operand(ctxt.to_owned(), reg!(R09))(h.to_owned())),
                    ins!(
                        Imulq,
                        lit!(size_ty(ctxt.clone().tdecls, (*t).clone()) as i64),
                        reg!(R09)
                    ),
                    ins!(Addq, reg!(R09), reg!(R08)),
                ];
                let mut acc_cpy = acc.to_vec();
                acc_cpy.extend(calculate_offset);
                gep2(ctxt, (*t).clone(), tl.to_vec(), acc_cpy)
            }
            _ => panic!("compile gep error"),
        }
    }
    match op {
        (Ty::Ptr(t), _) => gep2(ctxt, Ty::Array(0, t), path, base_addr),
        _ => panic!("compile gep must get a pointer"),
    }
}

// compiling instructions  --------------------------------------------------

// The result of compiling a single LLVM instruction might be many x86
//  instructions.  We have not determined the structure of this code
//  for you. Some of the instructions require only a couple of assembly
//  instructions, while others require more.  We have suggested that
//  you need at least compile_operand, compile_call, and compile_gep
//  helpers; you may introduce more as you see fit.

//  Here are a few notes:
//  - Icmp:  the Setb instruction may be of use.  Depending on how you
//    compile Cbr, you may want to ensure that the value produced by
//    Icmp is exactly 0 or 1.
//  - Load & Store: these need to dereference the pointers. Const and
//    Null operands aren't valid pointers.  Don't forget to
//    Platform.mangle the global identifier.
//  - Alloca: needs to return a pointer into the stack
//  - Bitcast: does nothing interesting at the assembly level

fn compile_insn(ctxt: Ctxt, (uid, i): (Uid, Insn)) -> Vec<x86::Ins> {
    use x86::{Operand::*, Reg::*};
    match i {
        Insn::Binop(op, _, a, b) => vec![
            compile_operand(ctxt.clone(), reg!(Rcx))(b),
            compile_operand(ctxt.clone(), reg!(R09))(a),
            ins!(compile_binop(op), reg!(Rcx), reg!(R09)),
            ins!(
                Movq,
                reg!(R09),
                ctxt.layout
                    .get(&uid)
                    .expect("could not lookup binout uid")
                    .to_owned()
            ),
        ],
        Insn::Icmp(cnd, _, a, b) => vec![
            compile_operand(ctxt.clone(), Reg(R08))(b),
            compile_operand(ctxt.clone(), Reg(R09))(a),
            ins!(Cmpq, reg!(R08), reg!(R09)),
            ins!(
                Movq,
                lit!(0),
                ctxt.layout
                    .get(&uid)
                    .expect("could not lookup binout uid")
                    .to_owned()
            ),
            ins!(
                x86::Opcode::Set(compile_cnd(cnd)),
                ctxt.layout
                    .get(&uid)
                    .expect("could not lookup binout uid")
                    .to_owned()
            ),
        ],
        Insn::Call(_, Operand::Gid(fun), ps) => compile_call(ctxt, &fun.0, uid, ps),
        Insn::Alloca(ty) => vec![
            ins!(Subq, lit!(size_ty(ctxt.tdecls, ty) as i64), reg!(Rsp)),
            ins!(
                Movq,
                reg!(Rsp),
                ctxt.layout
                    .get(&uid)
                    .expect("could not lookup binout uid")
                    .to_owned()
            ),
        ],
        Insn::Store(_, a, b) => vec![
            compile_operand(ctxt.clone(), Reg(R08))(a),
            compile_operand(ctxt, Reg(R09))(b),
            ins!(Movq, reg!(R08), ind_reg!(R09)),
        ],
        Insn::Load(_, a) => vec![
            compile_operand(ctxt.clone(), Reg(R08))(a),
            ins!(Movq, ind_reg!(R08), reg!(R09)),
            ins!(
                Movq,
                reg!(R09),
                ctxt.layout
                    .get(&uid)
                    .expect("could not lookup load uid")
                    .to_owned()
            ),
        ],
        Insn::Bitcast(_, op, _) => vec![
            compile_operand(ctxt.clone(), Reg(R08))(op),
            ins!(
                Movq,
                reg!(R08),
                ctxt.layout
                    .get(&uid)
                    .expect("could not lookup bitcast uid")
                    .to_owned()
            ),
        ],
        Insn::Gep(ty, op, path) => {
            let mut res = compile_gep(ctxt.clone(), (ty, op), path);
            res.extend(
                vec![ins!(
                    Movq,
                    reg!(R08),
                    ctxt.layout
                        .get(&uid)
                        .expect("could not lookup gep uid")
                        .to_owned()
                )]
                .into_iter(),
            );
            res
        }
        _ => panic!("compile_insn not implemented"),
    }
}

// compiling terminators  ---------------------------------------------------

// prefix the function name [fn] to a label to ensure that the X86 labels are
// globally unique.

fn mk_lbl(fn_n: &str, l: &str) -> String {
    format!("{}.{}", fn_n, l)
}

// Compile block terminators is not too difficult:
//    - Ret should properly exit the function: freeing stack space,
//      restoring the value of %rbp, and putting the return value (if
//      any) in %rax.
//    - Br should jump
//    - Cbr branch should treat its operand as a boolean conditional
//    [fn] - the name of the function containing this terminator

fn compile_terminator(fn_n: &str, ctxt: Ctxt, t: Terminator) -> Vec<x86::Ins> {
    use x86::{Opcode::*, Operand::*, Reg::*};

    let exit_code = vec![
        ins!(Movq, reg!(Rbp), Reg(Rsp)),
        ins!(Popq, reg!(Rbp)),
        ins!(Retq),
    ];
    match t {
        Terminator::Ret(Ty::Void, _) => exit_code,
        Terminator::Ret(_, Some(op)) => {
            let mut cmd = vec![compile_operand(ctxt, Reg(Rax))(op)];
            cmd.extend(exit_code);
            cmd
        }
        Terminator::Br(x) => vec![ins!(Jmp, lbl!(mk_lbl(fn_n, &x.0)))],
        Terminator::Cbr(op, lbl_t, lbl_f) => vec![
            compile_operand(ctxt, Reg(R08))(op),
            ins!(Cmpq, lit!(0), reg!(R08)),
            ins!(J(x86::Cnd::Neq), lbl!(mk_lbl(fn_n, &lbl_t.0))),
            ins!(Jmp, lbl!(mk_lbl(fn_n, &lbl_f.0))),
        ],
        _ => panic!("Return values not implemented"),
    }
}

// compiling blocks ---------------------------------------------------------

// We have left this helper function here for you to complete.
//  [fn] - the name of the function containing this block
//  [ctxt] - the current context
//  [blk]  - LLVM IR code for the block

fn compile_block(fn_n: &str, ctxt: Ctxt, blk: Block) -> Vec<x86::Ins> {
    let mut insts: Vec<x86::Ins> = blk
        .insns
        .into_iter()
        .flat_map(|x| compile_insn(ctxt.clone(), x))
        .collect();
    let term_ins = compile_terminator(fn_n, ctxt, blk.term.1);
    insts.extend(term_ins);
    insts
}

fn compile_lbl_block(fn_n: &str, lbl: &str, ctxt: &Ctxt, blk: Block) -> x86::Elem {
    x86::Elem::new(
        mk_lbl(fn_n, lbl),
        false,
        x86::Asm::Text(compile_block(fn_n, ctxt.clone(), blk)),
    )
}

// compile_fdecl ------------------------------------------------------------

// We suggest that you create a helper function that computes the
//  stack layout for a given function declaration.

//  - each function argument should be copied into a stack slot
//  - in this (inefficient) compilation strategy, each local id
//    is also stored as a stack slot.
//  - see the discussion about locals

fn stack_layout(args: Vec<Uid>, Cfg(block, lbled_blocks): Cfg) -> Layout {
    let uid2layout = |mut l: Layout, u: Uid| {
        l.insert(
            u,
            x86::Operand::Ind3(x86::Imm::Lit((l.len() + 1) as i64 * -8), x86::Reg::Rbp),
        );
        l
    };
    let custom_fold = |start_l: Layout, b: Block| {
        let l_int = b.insns.into_iter().fold(start_l, |l, x| uid2layout(l, x.0));
        uid2layout(l_int, b.term.0)
    };
    let l0 = Layout::new();
    let l1 = args.into_iter().fold(l0, uid2layout);
    let l2 = custom_fold(l1, block);
    lbled_blocks
        .into_iter()
        .fold(l2, |l, x| custom_fold(l, x.1))
}

// The code for the entry-point of a function must do several things:

//  - since our simple compiler maps local %uids to stack slots,
//    compiling the control-flow-graph body of an fdecl requires us to
//    compute the layout (see the discussion of locals and layout)

//  - the function code should also comply with the calling
//    conventions, typically by moving arguments out of the parameter
//    registers (or stack slots) into local storage space.  For our
//    simple compilation strategy, that local storage space should be
//    in the stack. (So the function parameters can also be accounted
//    for in the layout.)

//  - the function entry code should allocate the stack storage needed
//    to hold all of the local stack slots.

fn compile_fdecl(
    tdecls: HashMap<Tid, Ty>,
    name: &str,
    Fdecl {
        f_ty: _,
        f_param,
        f_cfg,
    }: Fdecl,
) -> x86::Prog {
    let layout = stack_layout(f_param.clone(), f_cfg.clone());
    let ctxt = Ctxt {
        tdecls,
        layout: layout.clone(),
    };

    let mut entry_code = vec![
        ins!(Pushq, reg!(Rbp)),
        ins!(Movq, reg!(Rsp), reg!(Rbp)),
        ins!(Subq, lit!((layout.len() as i64) * 8), reg!(Rsp)),
    ];

    let copy_params = f_param.into_iter().fold(vec![], |asm, param| {
        let mut new_ins = vec![
            ins!(Movq, arg_loc(asm.len() as i64 / 2), reg!(R10)),
            ins!(
                Movq,
                reg!(R10),
                layout
                    .get(&param)
                    .expect("could not find param in layout")
                    .to_owned()
            ),
        ];
        new_ins.extend(asm);
        new_ins
    });

    let compiled_block = compile_block(name, ctxt.clone(), f_cfg.0);
    let compiled_lbl_block = f_cfg
        .1
        .into_iter()
        .flat_map(|(x, y)| vec![compile_lbl_block(name, &x.0, &ctxt, y)]);

    entry_code.extend(copy_params);
    entry_code.extend(compiled_block);

    let mut pro = vec![x86::Elem {
        lbl: platform::mangle(name),
        global: (name == "main"),
        asm: x86::Asm::Text(entry_code),
    }];
    pro.extend(compiled_lbl_block);
    x86::Prog(pro)
}

// compile_gdecl ------------------------------------------------------------

// Compile a global value into an X86 global data declaration and map
// a global uid to its associated X86 label.

fn compile_ginit(g: Ginit) -> Vec<x86::Data> {
    match g {
        Ginit::GNull => vec![x86::Data::Quad(x86::Imm::Lit(0))],
        Ginit::GGid(gid) => vec![x86::Data::Quad(x86::Imm::Lbl(platform::mangle(&gid.0)))],
        Ginit::GInt(c) => vec![x86::Data::Quad(x86::Imm::Lit(c))],
        Ginit::GString(s) => vec![x86::Data::Asciz(s)],
        Ginit::GArray(gs) | Ginit::GStruct(gs) => gs.into_iter().flat_map(compile_gdecl).collect(),
        Ginit::GBitcast(_, g, _) => compile_ginit(*g),
    }
}

fn compile_gdecl(Gdecl(_, g): Gdecl) -> Vec<x86::Data> {
    compile_ginit(g)
}

// compile_prog -------------------------------------------------------------
pub fn compile_prog(
    Prog {
        tdecls,
        gdecls,
        fdecls,
        edecls: _,
    }: Prog,
) -> x86::Prog {
    let g = |(lbl, gdecl): (Gid, Gdecl)| {
        x86::Elem::new(
            platform::mangle(&lbl.0),
            true,
            x86::Asm::Data(compile_gdecl(gdecl)),
        )
    };
    let f = |(name, fdecl): (Gid, Fdecl)| compile_fdecl(tdecls.clone(), &name.0, fdecl).0;
    let mut cmpl_g_decl: Vec<x86::Elem> = gdecls.into_iter().map(g).collect();
    let cmpl_f_decl: Vec<x86::Elem> = fdecls.into_iter().flat_map(f).collect();
    cmpl_g_decl.extend(cmpl_f_decl);
    x86::Prog(cmpl_g_decl)
}
