#[macro_use]
extern crate lalrpop_util;
lalrpop_mod!(#[allow(clippy::all)] pub ll_parser);

pub mod backend;
// pub mod lexer;
pub mod llutil;
mod location;
pub mod platform;
pub mod tokens;
mod utils;

pub use crate::ll::*;

// LLVMlite: A simplified subset of LLVM IR

pub mod ll {
    use std::collections::HashMap;
    // Local identifiers
    #[derive(Clone, Debug, Hash, PartialEq, Eq)]
    pub struct Uid(pub String);

    // Global identifiers
    #[derive(Clone, Debug, Hash, PartialEq, Eq)]
    pub struct Gid(pub String);

    // Named types
    #[derive(Clone, Debug, Hash, PartialEq, Eq)]
    pub struct Tid(pub String);

    // Labels
    #[derive(Clone, Debug, Hash, PartialEq, Eq)]
    pub struct Lbl(pub String);

    // LLVM types
    #[derive(Clone, Debug, PartialEq)]
    pub enum Ty {
        Void,
        I1,
        I8,
        I64,
        Ptr(Box<Ty>),
        Struct(Vec<Box<Ty>>),
        Array(i64, Box<Ty>),
        Fun(Vec<Ty>, Box<Ty>),
        Namedt(Tid),
    }

    // Function type: argument types and return type
    pub type Fty = (Vec<Ty>, Ty);

    // Syntactic Values
    #[derive(Clone, Debug)]
    pub enum Operand {
        Null,
        Const(i64),
        Gid(Gid),
        Id(Uid),
    }

    // Binary i64 Operations
    #[derive(Clone, Copy, Debug)]
    pub enum Bop {
        Add,
        Sub,
        Mul,
        Shl,
        Lshr,
        Ashr,
        And,
        Or,
        Xor,
    }

    // Comparison Operators
    #[derive(Clone, Copy, Debug)]
    pub enum Cnd {
        Eq,
        Ne,
        Slt,
        Sle,
        Sgt,
        Sge,
    }

    // Instructions
    #[derive(Clone, Debug)]
    pub enum Insn {
        Binop(Bop, Ty, Operand, Operand),
        Alloca(Ty),
        Load(Ty, Operand),
        Store(Ty, Operand, Operand),
        Icmp(Cnd, Ty, Operand, Operand),
        Call(Ty, Operand, Vec<(Ty, Operand)>),
        Bitcast(Ty, Operand, Ty),
        Gep(Ty, Operand, Vec<Operand>),
    }

    #[derive(Clone, Debug)]
    pub enum Terminator {
        Ret(Ty, Option<Operand>),
        Br(Lbl),
        Cbr(Operand, Lbl, Lbl),
    }

    // Basic Blocks
    #[derive(Clone, Debug)]
    pub struct Block {
        pub insns: Vec<(Uid, Insn)>,
        pub term: (Uid, Terminator),
    }

    // Control Flow Graphs: entry and labeled blocks
    #[derive(Clone, Debug)]
    pub struct Cfg(pub Block, pub Vec<(Lbl, Block)>);

    // Function Declarations
    #[derive(Clone, Debug)]
    pub struct Fdecl {
        pub f_ty: Fty,
        pub f_param: Vec<Uid>,
        pub f_cfg: Cfg,
    }

    // Global Data Initializers
    #[derive(Clone, Debug)]
    pub enum Ginit {
        GNull,
        GGid(Gid),
        GInt(i64),
        GString(String),
        GArray(Vec<Gdecl>),
        GStruct(Vec<Gdecl>),
        GBitcast(Ty, Box<Ginit>, Ty),
    }

    // Global Declarations
    // pub type Gdecl = (Ty, Ginit);
    #[derive(Clone, Debug)]
    pub struct Gdecl(pub Ty, pub Ginit);

    // LLVMlite Programs
    #[derive(Clone, Debug)]
    pub struct Prog {
        pub tdecls: HashMap<Tid, Ty>,
        pub gdecls: HashMap<Gid, Gdecl>,
        pub fdecls: HashMap<Gid, Fdecl>,
        pub edecls: HashMap<Gid, Ty>,
    }

    impl Prog {
        pub fn new() -> Self {
            Prog {
                tdecls: HashMap::new(),
                gdecls: HashMap::new(),
                fdecls: HashMap::new(),
                edecls: HashMap::new(),
            }
        }
    }

    impl Default for Prog {
        fn default() -> Self {
            Prog::new()
        }
    }
}
