pub fn mangle(name: &str) -> String {
    if cfg!(unix) {
        return name.to_string();
    }
    format!("_{name}")
}

static mut SYM_CTR: u64 = 0;

pub fn gensym(name: &str) -> String {
    unsafe {
        SYM_CTR += 1;
        format!("_{name}__{SYM_CTR}")
    }
}
