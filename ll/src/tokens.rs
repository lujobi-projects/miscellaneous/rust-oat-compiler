use core::fmt;
use logos::{Lexer, Logos};

use crate::utils::{location::Location, range::Range};

#[derive(Logos, Clone, Debug, PartialEq)]
#[logos(skip r"[ \t\n\f]+")]
pub enum LlToken {
    #[regex(r"%\w*", id_to_string)]
    UID(String),

    #[regex(r"@\w*", id_to_string)]
    GID(String),

    #[regex("-?[0-9]+", |lex| lex.slice().parse().ok())]
    INT(i64),
    #[regex(r"[a-zA-Z_]\w*", |lex| lex.slice().to_string())]
    LBL(String),

    /* TODO: declare and comments
        #[token("")]
        COMMENT(String),
    */
    #[regex(r#"c"([^"\\\n]|\\.|\\\n)*""#, string_value)]
    STRING(String),

    #[token("*")]
    STAR,

    #[token(",")]
    COMMA,

    #[token(":")]
    COLON,

    #[token("=")]
    EQUALS,

    #[token("(")]
    LPAREN,

    #[token(")")]
    RPAREN,

    #[token("{")]
    LBRACE,

    #[token("}")]
    RBRACE,

    #[token("[")]
    LBRACKET,

    #[token("]")]
    RBRACKET,

    #[token("x")]
    CROSS,

    #[token("i1")]
    I1,

    #[token("i8")]
    I8,

    #[token("i32")]
    I32,

    #[token("i64")]
    I64,

    #[token("to")]
    TO,

    #[token("br")]
    BR,

    #[token("eq")]
    EQ,

    #[token("ne")]
    NE,

    #[token("or")]
    OR,

    #[token("and")]
    AND,

    #[token("add")]
    ADD,

    #[token("sub")]
    SUB,

    #[token("mul")]
    MUL,

    #[token("xor")]
    XOR,

    #[token("slt")]
    SLT,

    #[token("sle")]
    SLE,

    #[token("sgt")]
    SGT,

    #[token("sge")]
    SGE,

    #[token("shl")]
    SHL,

    #[token("ret")]
    RET,

    #[token("type")]
    TYPE,

    #[token("null")]
    NULL,

    #[token("lshr")]
    LSHR,

    #[token("ashr")]
    ASHR,

    #[token("call")]
    CALL,

    #[token("icmp")]
    ICMP,

    #[token("void")]
    VOID,

    #[token("load")]
    LOAD,

    #[token("store")]
    STORE,

    #[token("label")]
    LABEL,

    #[token("entry")]
    ENTRY,

    #[token("global")]
    GLOBAL,

    #[token("define")]
    DEFINE,

    #[token("declare")]
    DECLARE,

    #[token("external")]
    EXTERNAL,

    #[token("alloca")]
    ALLOCA,

    #[token("bitcast")]
    BITCAST,

    #[token("getelementptr")]
    GEP,

    // #[token("")]
    EOF,
}

fn id_to_string(lex: &mut Lexer<LlToken>) -> Option<String> {
    let s = lex.slice();
    Some(s[1..s.len()].to_string())
}

fn string_value(lex: &mut Lexer<LlToken>) -> Option<String> {
    let s = lex.slice();
    let s = s.trim_start_matches('c');
    let s = s.trim_start_matches('"');
    let s = s.trim_end_matches('"');
    Some(s.to_string())
}

impl fmt::Display for LlToken {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            LlToken::STAR => f.write_str("*"),
            LlToken::COMMA => f.write_str(","),
            LlToken::COLON => f.write_str(":"),
            LlToken::EQUALS => f.write_str("="),
            LlToken::LPAREN => f.write_str("("),
            LlToken::RPAREN => f.write_str(")"),
            LlToken::LBRACE => f.write_str("{"),
            LlToken::RBRACE => f.write_str("}"),
            LlToken::LBRACKET => f.write_str("["),
            LlToken::RBRACKET => f.write_str("]"),
            LlToken::EOF => f.write_str(""),
            LlToken::CROSS => f.write_str("x"),
            LlToken::I1 => f.write_str("i1"),
            LlToken::I8 => f.write_str("i8"),
            LlToken::I32 => f.write_str("i32"),
            LlToken::I64 => f.write_str("i64"),
            LlToken::TO => f.write_str("to"),
            LlToken::BR => f.write_str("br"),
            LlToken::EQ => f.write_str("eq"),
            LlToken::NE => f.write_str("ne"),
            LlToken::OR => f.write_str("or"),
            LlToken::AND => f.write_str("and"),
            LlToken::ADD => f.write_str("add"),
            LlToken::SUB => f.write_str("sub"),
            LlToken::MUL => f.write_str("mul"),
            LlToken::XOR => f.write_str("xor"),
            LlToken::SLT => f.write_str("slt"),
            LlToken::SLE => f.write_str("sle"),
            LlToken::SGT => f.write_str("sgt"),
            LlToken::SGE => f.write_str("sge"),
            LlToken::SHL => f.write_str("shl"),
            LlToken::RET => f.write_str("ret"),
            LlToken::TYPE => f.write_str("type"),
            LlToken::NULL => f.write_str("null"),
            LlToken::LSHR => f.write_str("lshr"),
            LlToken::ASHR => f.write_str("ashr"),
            LlToken::CALL => f.write_str("call"),
            LlToken::ICMP => f.write_str("icmp"),
            LlToken::VOID => f.write_str("void"),
            LlToken::LOAD => f.write_str("load"),
            LlToken::STORE => f.write_str("store"),
            LlToken::LABEL => f.write_str("label"),
            LlToken::ENTRY => f.write_str("entry"),
            LlToken::GLOBAL => f.write_str("global"),
            LlToken::DEFINE => f.write_str("define"),
            LlToken::DECLARE => f.write_str("declare"),
            LlToken::EXTERNAL => f.write_str("external"),
            LlToken::ALLOCA => f.write_str("alloca"),
            LlToken::BITCAST => f.write_str("bitcast"),
            LlToken::GEP => f.write_str("getelementptr"),
            LlToken::UID(value) => write!(f, "UID({value})"),
            LlToken::GID(value) => write!(f, "GID({value})"),
            LlToken::INT(value) => write!(f, "INT({value})"),
            LlToken::LBL(value) => write!(f, "LBL({value})"),
            // LlToken::COMMENT { value } => write!(f, "COMMENT({value})"),
            LlToken::STRING(value) => write!(f, "STRING({value})"), // LlToken::EOF => f.write_str(""),
                                                                    // LlToken::Error => write!(f, "ERROR"),
        }
    }
}

pub fn make_tokenizer(source: &str) -> impl Iterator<Item = LexResult> + '_ {
    // make_tokenizer_located(source, Location::new(0, 0))
    LlLexer::new(source)
}

pub type Spanned = (Location, LlToken, Location);
pub type LexResult = Result<Spanned, LexicalError>;

#[derive(Debug, Clone)]
pub enum LexicalError {
    // UnrecognizedToken(Range),
    // UnexpectedToken(Range, char),
    UnbalancedBrackets(Range, i32),
    // Not possible
}

impl std::fmt::Display for LexicalError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            // LexicalError::UnrecognizedToken(loc) => write!(f, "Unrecognized token at {loc}"),
            // LexicalError::UnexpectedToken(loc, c) => {
            //     write!(f, "Unexpected token '{c}' at {loc}")
            // }
            LexicalError::UnbalancedBrackets(loc, n) => {
                write!(f, "Unbalanced brackets at {loc} (n={n})")
            }
        }
    }
}

pub struct LlLexer<'source> {
    internal_lexer: logos::SpannedIter<'source, LlToken>,
    nesting: i32,
    eof_reached: bool,
    newlines: Vec<usize>,
}

impl<'source> LlLexer<'source> {
    pub fn new(input: &'source str) -> Self {
        // count the characters on each line of the input
        let mut newlines = Vec::new();
        for (i, c) in input.char_indices() {
            if c == '\n' {
                newlines.push(i);
            }
        }
        newlines.push(input.len());
        LlLexer {
            internal_lexer: LlToken::lexer(input).spanned(),
            nesting: 0,
            eof_reached: false,
            newlines,
        }
    }

    fn find_index_of_pos(&self, pos: usize) -> Location {
        let mut prev_newline = 0;
        for (i, &newline) in self.newlines.iter().enumerate() {
            if newline > pos {
                return Location::new(i, pos - prev_newline);
            }
            prev_newline = newline;
        }
        Location::new(0, 0)
    }
}

impl<'source> Iterator for LlLexer<'source> {
    type Item = LexResult;

    fn next(&mut self) -> Option<Self::Item> {
        if self.eof_reached {
            return None;
        }
        match self.internal_lexer.next() {
            Some((token, range)) => {
                if token == Ok(LlToken::LBRACE) {
                    self.nesting += 1;
                } else if token == Ok(LlToken::RBRACE) {
                    self.nesting -= 1;
                }
                if self.nesting < 0 {
                    return Some(Err(LexicalError::UnbalancedBrackets(
                        Range(
                            token.unwrap().to_string(),
                            self.find_index_of_pos(range.start),
                            self.find_index_of_pos(range.end),
                        ),
                        self.nesting,
                    )));
                }
                Some(Ok((
                    self.find_index_of_pos(range.start),
                    token.unwrap(),
                    self.find_index_of_pos(range.end),
                )))
            }
            None => {
                self.eof_reached = true;
                Some(Ok((Location::new(0, 0), LlToken::EOF, Location::new(0, 0))))
            }
        }
    }
}
