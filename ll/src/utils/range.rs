use super::location::Location;

#[derive(Clone, Debug, PartialEq)]
pub struct Range(pub String, pub Location, pub Location);

impl Range {
    pub fn new(st: String, start: Location, end: Location) -> Self {
        Range(st, start, end)
    }

    pub fn no_range() -> Self {
        Range(
            "__internal".to_owned(),
            Location::new(0, 0),
            Location::new(0, 0),
        )
    }
}

impl std::fmt::Display for Range {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "(\"{}\", ({}), ({}))", self.0, self.1, self.2)
    }
}
