use crate::utils::range::Range;

#[derive(Clone, Debug, PartialEq)]
pub struct Node<T>
where
    T: std::fmt::Display,
{
    pub elt: Box<T>,
    pub loc: Range,
}

impl<T> Node<T>
where
    T: std::fmt::Display,
{
    pub fn new(elt: Box<T>, loc: Range) -> Self {
        Node { elt, loc }
    }
    pub fn no_loc(elt: Box<T>) -> Self {
        Node {
            elt,
            loc: Range::no_range(),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Hash, Eq)]
pub struct Id(pub String);

#[derive(Clone, Debug, PartialEq)]
pub enum RetTy {
    Void,
    Val(Ty),
}

#[derive(Clone, Debug, PartialEq)]
pub enum Rty {
    String,
    Array(Ty),
    Fun(Vec<Ty>, RetTy),
}

#[derive(Clone, Debug, PartialEq)]
pub enum Ty {
    Bool,
    Int,
    Ref(Box<Rty>),
}

#[derive(Clone, Debug, PartialEq)]
pub enum Unop {
    Neg,
    Lognot,
    Bitnot,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Binop {
    Add,
    Sub,
    Mul,
    Eq,
    Neq,
    Lt,
    Lte,
    Gt,
    Gte,
    And,
    Or,
    IAnd,
    IOr,
    Shl,
    Shr,
    Sar,
}

#[derive(Clone, Debug, PartialEq)]
pub struct CallTy(pub Node<Exp>, pub Vec<Node<Exp>>);

#[derive(Clone, Debug, PartialEq)]
pub enum Exp {
    CNull(Rty),
    CBool(bool),
    CInt(i64),
    CStr(String),
    CArr(Ty, Vec<Node<Exp>>),
    NewArr(Ty, Node<Exp>),
    Id(Id),
    Index(Node<Exp>, Node<Exp>),
    Call(CallTy),
    Bop(Binop, Node<Exp>, Node<Exp>),
    Uop(Unop, Node<Exp>),
}

#[derive(Clone, Debug, PartialEq)]
pub struct Cfield(pub Id, pub Node<Exp>);

#[derive(Clone, Debug, PartialEq)]
pub struct Vdecl(pub Id, pub Node<Exp>);

#[derive(Clone, Debug, PartialEq)]
pub enum Stmt {
    Assn(Node<Exp>, Node<Exp>),
    Decl(Vdecl),
    Ret(Option<Node<Exp>>),
    SCall(CallTy),
    If(Node<Exp>, Block, Block),
    For(Vec<Vdecl>, Option<Node<Exp>>, Option<Node<Stmt>>, Block),
    While(Node<Exp>, Block),
}

#[derive(Clone, Debug, PartialEq)]
pub struct Block(pub Vec<Node<Stmt>>);

#[derive(Clone, Debug, PartialEq)]
pub struct Gdecl {
    pub name: Id,
    pub init: Box<Node<Exp>>,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Arg(pub Ty, pub Id);

#[derive(Clone, Debug, PartialEq)]
pub struct Fdecl {
    pub frtyp: RetTy,
    pub fname: Id,
    pub args: Vec<Arg>,
    pub body: Block,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Field {
    pub field_name: Id,
    pub ftyp: Ty,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Decl {
    Gvdecl(Node<Gdecl>),
    Gfdecl(Node<Fdecl>),
}

#[derive(Clone, Debug, PartialEq)]
pub struct Prog(pub Vec<Decl>);

pub trait NoLocable {
    fn reset_nodes(&self) -> Self;
}
