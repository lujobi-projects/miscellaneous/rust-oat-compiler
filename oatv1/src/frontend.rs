use core::panic;
use std::collections::HashMap;
use std::vec;

use crate::{ast, ptr_ty};
use ll::ll as llvm;
use ll::platform;

// instruction streams ------------------------------------------------------

/* As in the last project, we'll be working with a flattened representation
   of LLVMlite programs to make emitting code easier. This version
   additionally makes it possible to emit elements will be gathered up and
   "hoisted" to specific parts of the constructed CFG
   - G of gid * Ll.gdecl: allows you to output global definitions in the middle
     of the instruction stream. You will find this useful for compiling string
     literals
   - E of uid * insn: allows you to emit an instruction that will be moved up
     to the entry block of the current function. This will be useful for
     compiling local variable declarations
*/
#[derive(Debug, Clone)]
enum Elt {
    L(llvm::Lbl),              // block labels
    I(llvm::Uid, llvm::Insn),  // instruction
    T(llvm::Terminator),       // block terminator
    G(llvm::Gid, llvm::Gdecl), // hoisted globals (usually strings)
    E(llvm::Uid, llvm::Insn),  // hoisted entry block instructions
}

type Stream = Vec<Elt>;

fn lift(ins: Vec<(llvm::Uid, llvm::Insn)>) -> Stream {
    ins.into_iter()
        .map(|(uid, insn)| Elt::I(uid, insn))
        .collect()
}

// Build a CFG and collection of global variable definitions from a stream
fn cfg_of_stream(code: Stream) -> (llvm::Cfg, Vec<(llvm::Gid, llvm::Gdecl)>) {
    let (gs, mut einsns, mut insns, term_opt, mut blks) = code.into_iter().rev().fold(
        (Vec::new(), Vec::new(), Vec::new(), None, Vec::new()),
        |(mut gs, mut einsns, mut insns, term_opt, mut blks), elt| match elt {
            Elt::L(lbl) => match term_opt {
                None => {
                    if insns.is_empty() {
                        return (gs, einsns, Vec::new(), None, blks);
                    }
                    panic!("build_cfg: block labeled {lbl} has no terminator")
                }
                Some(term) => {
                    blks.push((
                        lbl,
                        llvm::Block {
                            insns: insns.into_iter().rev().collect(),
                            term,
                        },
                    ));
                    (gs, einsns, Vec::new(), None, blks)
                }
            },
            Elt::T(term) => (
                gs,
                einsns,
                insns,
                Some((ll::ll::Uid(platform::gensym("tmn")), term)),
                blks,
            ),
            Elt::I(uid, insn) => {
                // insns.insert(0, (uid, insn));
                insns.push((uid, insn));
                (gs, einsns, insns, term_opt, blks)
            }
            Elt::G(gid, gdecl) => {
                // gs.insert(0, (gid, gdecl));
                gs.push((gid, gdecl));
                (gs, einsns, insns, term_opt, blks)
            }
            Elt::E(uid, insn) => {
                // einsns.insert(0, (uid, insn));
                einsns.push((uid, insn));
                (gs, einsns, insns, term_opt, blks)
            }
        },
    );
    blks.reverse();
    match term_opt {
        None => panic!("build_cfg: entry block has no terminator"),
        Some(term) => {
            einsns.reverse();
            insns.reverse();
            einsns.append(&mut insns);
            (
                llvm::Cfg(
                    llvm::Block {
                        insns: einsns,
                        term,
                    },
                    blks,
                ),
                gs,
            )
        }
    }
}

// compilation contexts -----------------------------------------------------

/* To compile OAT variables, we maintain a mapping of source identifiers to the
corresponding LLVMlite operands. Bindings are added for global OAT variables
and local variables that are in scope. */

type Ctxt = HashMap<llvm::Gid, (llvm::Ty, llvm::Operand)>;

fn lookup_function(ctxt: &Ctxt, id: &ast::Id) -> (llvm::Ty, llvm::Operand) {
    if let Some((llvm::Ty::Ptr(ptr), g)) = ctxt.get(&llvm::Gid(id.clone().0)) {
        if let llvm::Ty::Fun(args, ret) = *ptr.to_owned() {
            return (
                llvm::Ty::Ptr(Box::new(llvm::Ty::Fun(args, ret))),
                g.to_owned(),
            );
        }
    }
    panic!("{id} not bound to a function")
}

// compiling OAT types ------------------------------------------------------

/* The mapping of source types onto LLVMlite is straightforward. Booleans and ints
are represented as the corresponding integer types. OAT strings are
pointers to bytes (I8). Arrays are the most interesting type: they are
represented as pointers to structs where the first component is the number
of elements in the following array.

The trickiest part of this project will be satisfying LLVM's rudimentary type
system. Recall that global arrays in LLVMlite need to be declared with their
length in the type to statically allocate the right amount of memory. The
global strings and arrays you emit will therefore have a more specific type
annotation than the output of cmp_rty. You will have to carefully bitcast
gids to satisfy the LLVM type checker. */

fn cmp_ty(ty: &ast::Ty) -> llvm::Ty {
    match ty {
        ast::Ty::Bool => llvm::Ty::I1,
        ast::Ty::Int => llvm::Ty::I64,
        ast::Ty::Ref(r) => llvm::Ty::Ptr(Box::new(cmp_rty(r))),
    }
}

fn cmp_rty(rty: &ast::Rty) -> llvm::Ty {
    match rty {
        ast::Rty::String => llvm::Ty::I8,
        ast::Rty::Array(u) => llvm::Ty::Struct(vec![
            Box::new(llvm::Ty::I64),
            Box::new(llvm::Ty::Array(0, Box::new(cmp_ty(u)))),
        ]),
        ast::Rty::Fun(ts, t) => {
            let (args, ret) = cmp_fty(ts, t);
            llvm::Ty::Fun(args, Box::new(ret))
        }
    }
}

fn cmp_ret_ty(ret_ty: &ast::RetTy) -> llvm::Ty {
    match ret_ty {
        ast::RetTy::Void => llvm::Ty::Void,
        ast::RetTy::Val(t) => cmp_ty(t),
    }
}

fn cmp_fty(ts: &[ast::Ty], r: &ast::RetTy) -> llvm::Fty {
    (ts.iter().map(cmp_ty).collect(), cmp_ret_ty(r))
}

fn typ_of_binop(bop: &ast::Binop) -> (ast::Ty, ast::Ty, ast::Ty) {
    match bop {
        ast::Binop::Add
        | ast::Binop::Mul
        | ast::Binop::Sub
        | ast::Binop::Shl
        | ast::Binop::Shr
        | ast::Binop::Sar
        | ast::Binop::IAnd
        | ast::Binop::IOr => (ast::Ty::Int, ast::Ty::Int, ast::Ty::Int),
        ast::Binop::Eq
        | ast::Binop::Neq
        | ast::Binop::Lt
        | ast::Binop::Lte
        | ast::Binop::Gt
        | ast::Binop::Gte => (ast::Ty::Int, ast::Ty::Int, ast::Ty::Bool),
        ast::Binop::And | ast::Binop::Or => (ast::Ty::Bool, ast::Ty::Bool, ast::Ty::Bool),
    }
}

fn typ_of_unop(uop: &ast::Unop) -> (ast::Ty, ast::Ty) {
    match uop {
        ast::Unop::Neg | ast::Unop::Bitnot => (ast::Ty::Int, ast::Ty::Int),
        ast::Unop::Lognot => (ast::Ty::Bool, ast::Ty::Bool),
    }
}

fn cmp_binop(bop: &ast::Binop) -> llvm::Bop {
    match bop {
        ast::Binop::Add => llvm::Bop::Add,
        ast::Binop::Mul => llvm::Bop::Mul,
        ast::Binop::Sub => llvm::Bop::Sub,
        ast::Binop::Shl => llvm::Bop::Shl,
        ast::Binop::Shr => llvm::Bop::Lshr,
        ast::Binop::Sar => llvm::Bop::Ashr,
        ast::Binop::IAnd => llvm::Bop::And,
        ast::Binop::IOr => llvm::Bop::Or,
        ast::Binop::And => llvm::Bop::And,
        ast::Binop::Or => llvm::Bop::Or,
        _ => panic!("Not a LL bop type"),
    }
}

fn cmp_cdn(cdn: &ast::Binop) -> llvm::Cnd {
    match cdn {
        ast::Binop::Eq => llvm::Cnd::Eq,
        ast::Binop::Neq => llvm::Cnd::Ne,
        ast::Binop::Lt => llvm::Cnd::Slt,
        ast::Binop::Lte => llvm::Cnd::Sle,
        ast::Binop::Gt => llvm::Cnd::Sgt,
        ast::Binop::Gte => llvm::Cnd::Sge,
        _ => panic!("Not a LL cnd type"),
    }
}

fn cmp_unop(uop: &ast::Unop) -> (llvm::Operand, llvm::Bop) {
    match uop {
        ast::Unop::Neg => (llvm::Operand::Const(0), llvm::Bop::Sub),
        ast::Unop::Bitnot => (llvm::Operand::Const(-1), llvm::Bop::Xor),
        ast::Unop::Lognot => (llvm::Operand::Const(1), llvm::Bop::Xor),
    }
}

/* Compiler Invariants

   The LLVM IR type of a variable (whether global or local) that stores an Oat
   array value (or any other reference type, like "string") will always be a
   double pointer.  In general, any Oat variable of Oat-type t will be
   represented by an LLVM IR value of type Ptr (cmp_ty t).  So the Oat variable
   x : int will be represented by an LLVM IR value of type i64*, y : string will
   be represented by a value of type i8**, and arr : int[] will be represented
   by a value of type {i64, [0 x i64]}**.  Whether the LLVM IR type is a
   "single" or "double" pointer depends on whether t is a reference type.

   We can think of the compiler as paying careful attention to whether a piece
   of Oat syntax denotes the "value" of an expression or a pointer to the
   "storage space associated with it".  This is the distinction between an
   "expression" and the "left-hand-side" of an assignment statement.  Compiling
   an Oat variable identifier as an expression ("value") does the load, so
   cmp_exp called on an Oat variable of type t returns (code that) generates a
   LLVM IR value of type cmp_ty t.  Compiling an identifier as a left-hand-side
   does not do the load, so cmp_lhs called on an Oat variable of type t returns
   and operand of type (cmp_ty t)*.  Extending these invariants to account for
   array accesses: the assignment e1[e2] = e3; treats e1[e2] as a
   left-hand-side, so we compile it as follows: compile e1 as an expression to
   obtain an array value (which is of pointer of type {i64, [0 x s]}* ).
   compile e2 as an expression to obtain an operand of type i64, generate code
   that uses getelementptr to compute the offset from the array value, which is
   a pointer to the "storage space associated with e1[e2]".

   On the other hand, compiling e1[e2] as an expression (to obtain the value of
   the array), we can simply compile e1[e2] as a left-hand-side and then do the
   load.  So cmp_exp and cmp_lhs are mutually recursive.  [[Actually, as I am
   writing this, I think it could make sense to factor the Oat grammar in this
   way, which would make things clearer, I may do that for next time around.]]


   Consider globals7.oat

   /--------------- globals7.oat ------------------
   global arr = int[] null;

   int foo() {
     var x = new int[3];
     arr = x;
     x[2] = 3;
     return arr[2];
   }
   /------------------------------------------------

   The translation (given by cmp_ty) of the type int[] is {i64, [0 x i64}* so
   the corresponding LLVM IR declaration will look like:

   @arr = global { i64, [0 x i64] }* null

   This means that the type of the LLVM IR identifier @arr is {i64, [0 x i64]}**
   which is consistent with the type of a locally-declared array variable.

   The local variable x would be allocated and initialized by (something like)
   the following code snippet.  Here %_x7 is the LLVM IR uid containing the
   pointer to the "storage space" for the Oat variable x.

   %_x7 = alloca { i64, [0 x i64] }*                              ;; (1)
   %_raw_array5 = call i64*  @oat_alloc_array(i64 3)              ;; (2)
   %_array6 = bitcast i64* %_raw_array5 to { i64, [0 x i64] }*    ;; (3)
   store { i64, [0 x i64]}* %_array6, { i64, [0 x i64] }** %_x7   ;; (4)

   (1) note that alloca uses cmp_ty (int[]) to find the type, so %_x7 has
       the same type as @arr

   (2) @oat_alloc_array allocates len+1 i64's

   (3) we have to bitcast the result of @oat_alloc_array so we can store it
        in %_x7

   (4) stores the resulting array value (itself a pointer) into %_x7

  The assignment arr = x; gets compiled to (something like):

  %_x8 = load { i64, [0 x i64] }*, { i64, [0 x i64] }** %_x7     ;; (5)
  store {i64, [0 x i64] }* %_x8, { i64, [0 x i64] }** @arr       ;; (6)

  (5) load the array value (a pointer) that is stored in the address pointed
      to by %_x7

  (6) store the array value (a pointer) into @arr

  The assignment x[2] = 3; gets compiled to (something like):

  %_x9 = load { i64, [0 x i64] }*, { i64, [0 x i64] }** %_x7      ;; (7)
  %_index_ptr11 = getelementptr { i64, [0 x  i64] },
                  { i64, [0 x i64] }* %_x9, i32 0, i32 1, i32 2   ;; (8)
  store i64 3, i64* %_index_ptr11                                 ;; (9)

  (7) as above, load the array value that is stored %_x7

  (8) calculate the offset from the array using GEP

  (9) store 3 into the array

  Finally, return arr[2]; gets compiled to (something like) the following.
  Note that the way arr is treated is identical to x.  (Once we set up the
  translation, there is no difference between Oat globals and locals, except
  how their storage space is initially allocated.)

  %_arr12 = load { i64, [0 x i64] }*, { i64, [0 x i64] }** @arr    ;; (10)
  %_index_ptr14 = getelementptr { i64, [0 x i64] },
                 { i64, [0 x i64] }* %_arr12, i32 0, i32 1, i32 2  ;; (11)
  %_index15 = load i64, i64* %_index_ptr14                         ;; (12)
  ret i64 %_index15

  (10) just like for %_x9, load the array value that is stored in @arr

  (11)  calculate the array index offset

  (12) load the array value at the index

*/

/* Global initialized arrays:

  There is another wrinkle: To compile global initialized arrays like in the
  globals4.oat, it is helpful to do a bitcast once at the global scope to
  convert the "precise type" required by the LLVM initializer to the actual
  translation type (which sets the array length to 0).  So for globals4.oat,
  the arr global would compile to (something like):

  @arr = global { i64, [0 x i64] }* bitcast
           ({ i64, [4 x i64] }* @_global_arr5 to { i64, [0 x i64] }* )
  @_global_arr5 = global { i64, [4 x i64] }
                  { i64 4, [4 x i64] [ i64 1, i64 2, i64 3, i64 4 ] }

*/

// Some useful helper functions

/* Generate a fresh temporary identifier. Since OAT identifiers cannot begin
with an underscore, these should not clash with any source variables */
fn gensym(s: &str) -> String {
    format!("_{}", platform::gensym(s))
}

/* Amount of space an Oat type takes when stored in the stack, in bytes.
Note that since structured values are manipulated by reference, all
Oat values take 8 bytes on the stack. */
#[allow(dead_code)]
fn size_oat_ty(_t: ast::Ty) -> i64 {
    8
}

/* Generate code to allocate a zero-initialized array of source type TRef (RArray t) of the
given size. Note "size" is an operand whose value can be computed at
runtime */
fn oat_alloc_array(t: ast::Ty, size: llvm::Operand) -> (llvm::Ty, llvm::Operand, Stream) {
    let ans_id = gensym("array");
    let arr_id = gensym("raw_array");
    let ans_ty = cmp_ty(&ast::Ty::Ref(Box::new(ast::Rty::Array(t))));
    let arr_ty = llvm::Ty::Ptr(Box::new(llvm::Ty::I64));
    (
        ans_ty.clone(),
        llvm::Operand::Id(llvm::Uid(ans_id.clone())),
        lift(vec![
            (
                llvm::Uid(arr_id.clone()),
                llvm::Insn::Call(
                    arr_ty.clone(),
                    llvm::Operand::Gid(llvm::Gid("oat_alloc_array".to_owned())),
                    vec![(llvm::Ty::I64, size)],
                ),
            ),
            (
                llvm::Uid(ans_id),
                llvm::Insn::Bitcast(arr_ty, llvm::Operand::Id(llvm::Uid(arr_id)), ans_ty),
            ),
        ]),
    )
}

/* Compiles an expression exp in context c, outputting the Ll operand that will
recieve the value of the expression, and the stream of instructions
implementing the expression.

Tips:
- use the provided cmp_ty function!

- string literals (CStr s) should be hoisted. You'll need to make sure
  either that the resulting gid has type (Ptr I8), or, if the gid has type
  [n x i8] (where n is the length of the string), convert the gid to a
  (Ptr I8), e.g., by using getelementptr.

- use the provided "oat_alloc_array" function to implement literal arrays
  (CArr) and the (NewArr) expressions
*/

fn extr_ptr(ty: &llvm::Ty) -> llvm::Ty {
    match ty {
        llvm::Ty::Ptr(t) => *t.clone(),
        _ => panic!("extr_ptr: expected a pointer type, got: {ty}"),
    }
}

fn extr_arr_t(t: &llvm::Ty) -> llvm::Ty {
    if let llvm::Ty::Ptr(ptr) = t.clone() {
        if let llvm::Ty::Struct(tys) = *ptr {
            if let [_, arr] = tys.as_slice() {
                if let llvm::Ty::Array(_, t) = arr.as_ref() {
                    return t.as_ref().clone();
                }
            }
        }
    }
    panic!("Can only load from array type, got: {t}");
}

fn cmp_exp(c: &Ctxt, exp: &ast::Node<ast::Exp>) -> (llvm::Ty, llvm::Operand, Stream) {
    use llvm::*;
    match exp.elt.as_ref().clone() {
        ast::Exp::CNull(rt) => (cmp_rty(&rt), llvm::Operand::Null, Stream::new()),
        ast::Exp::CBool(b) => (Ty::I1, Operand::Const(if b { 1 } else { 0 }), Stream::new()),
        ast::Exp::CInt(i) => (Ty::I64, Operand::Const(i), Stream::new()),
        ast::Exp::CStr(s) => {
            let (sym1, sym2, sym3) = (gensym("string"), gensym("string"), gensym("string"));
            let str_t = Ty::Array(s.len() as i64 + 1, Box::new(Ty::I8));
            let target_decl = (
                ptr_ty!(Ty::I8),
                Ginit::GBitcast(
                    ptr_ty!(str_t),
                    Box::new(Ginit::GGid(llvm::Gid(sym1.clone()))),
                    ptr_ty!(Ty::I8),
                ),
            );
            (
                ptr_ty!(Ty::I8),
                Operand::Id(Uid(sym3.clone())),
                vec![
                    Elt::G(Gid(sym2.clone()), Gdecl(target_decl.0, target_decl.1)),
                    Elt::G(
                        Gid(sym1),
                        Gdecl(
                            Ty::Array(s.len() as i64 + 1, Box::new(Ty::I8)),
                            Ginit::GString(s),
                        ),
                    ),
                    Elt::I(
                        Uid(sym3),
                        Insn::Load(ptr_ty!(ptr_ty!(Ty::I8)), Operand::Gid(Gid(sym2))),
                    ),
                ],
            )
        }
        ast::Exp::CArr(t, exps) => {
            let (t1, o1, s1) = oat_alloc_array(t, Operand::Const(exps.len() as i64));
            let t1_clone = t1.clone();
            let o1_clone = o1.clone();

            let insns = exps.into_iter().enumerate().map(|(i, exp)| {
                let (t, o, mut s) = cmp_exp(c, &exp);
                let sym = gensym("");
                let mut gep = vec![Elt::I(
                    Uid(sym.clone()),
                    Insn::Gep(
                        t1_clone.clone(),
                        o1_clone.clone(),
                        vec![
                            Operand::Const(0),
                            Operand::Const(1),
                            Operand::Const(i as i64),
                        ],
                    ),
                )];
                s.append(&mut gep);
                s.push(Elt::I(
                    Uid("".to_owned()),
                    Insn::Store(t, o, Operand::Id(Uid(sym))),
                ));
                s
            });
            // (t1, o1, s1.into_iter().chain(insns.flatten()).collect())

            (t1, o1, s1.into_iter().chain(insns.flatten()).collect())
        }
        ast::Exp::NewArr(t, exp) => {
            let (_t1, o1, s1) = cmp_exp(c, &exp);
            let (t2, o2, s2) = oat_alloc_array(t, o1);
            (t2, o2, s1.into_iter().chain(s2).collect())
        }
        ast::Exp::Id(id) => {
            let (t, o) = c.get(&llvm::Gid(id.clone().0)).unwrap();
            let sym = gensym(id.0.as_str());
            (
                extr_ptr(t),
                Operand::Id(Uid(sym.clone())),
                vec![Elt::I(Uid(sym), Insn::Load(t.to_owned(), o.to_owned()))],
            )
        }
        ast::Exp::Bop(op, exp1, exp2) => {
            let (t1, o1, s1) = cmp_exp(c, &exp1);
            let (_t2, o2, s2) = cmp_exp(c, &exp2);
            let (t_bop_l, _t_bop_r, t_bop) = typ_of_binop(&op);
            let sym = gensym("");
            if t_bop == ast::Ty::Int || t_bop_l == ast::Ty::Bool {
                return (
                    cmp_ty(&t_bop),
                    Operand::Id(Uid(sym.clone())),
                    s1.into_iter()
                        .chain(s2)
                        .chain(vec![Elt::I(
                            Uid(sym),
                            Insn::Binop(cmp_binop(&op), cmp_ty(&t_bop), o1, o2),
                        )])
                        .collect(),
                );
            }
            (
                cmp_ty(&t_bop),
                Operand::Id(Uid(sym.clone())),
                s1.into_iter()
                    .chain(s2)
                    .chain(vec![Elt::I(Uid(sym), Insn::Icmp(cmp_cdn(&op), t1, o1, o2))])
                    .collect(),
            )
        }
        ast::Exp::Uop(op, exp) => {
            let (_t, o, s) = cmp_exp(c, &exp);
            let (_tpy_l, t_bop) = typ_of_unop(&op);
            let (o1, llop) = cmp_unop(&op);
            let sym = gensym("");
            (
                cmp_ty(&t_bop),
                Operand::Id(Uid(sym.clone())),
                s.into_iter()
                    .chain(vec![Elt::I(
                        Uid(sym),
                        Insn::Binop(llop, cmp_ty(&t_bop), o1, o),
                    )])
                    .collect(),
            )
        }
        ast::Exp::Index(_, _) => {
            let (ty, op, mut str) = cmp_lhs(c, exp);
            let sym = gensym("");
            let mut ins = vec![Elt::I(Uid(sym.clone()), Insn::Load(ty.clone(), op))];
            str.append(&mut ins);
            (extr_ptr(&ty), Operand::Id(Uid(sym)), str)
        }
        ast::Exp::Call(ast::CallTy(
            ast::Node {
                elt: call_id_ptr, ..
            },
            args,
        )) => {
            if let ast::Exp::Id(fn_n) = call_id_ptr.as_ref() {
                let (fct_ptr, func_op) = lookup_function(c, fn_n);
                let ty = if let Ty::Ptr(x) = fct_ptr.clone() {
                    if let Ty::Fun(_, y) = *x {
                        y
                    } else {
                        panic!("expected function pointer");
                    }
                } else {
                    panic!("expected pointer type");
                };
                let (argslist, mut s) =
                    args.into_iter()
                        .fold((vec![], vec![]), |(mut args_, s), exp| {
                            let (t, o, sn) = cmp_exp(c, &exp);
                            args_.push((t, o));
                            (args_, s.into_iter().chain(sn).collect())
                        });
                let sym = gensym("");
                let tmp = Elt::I(
                    Uid(sym.clone()),
                    Insn::Call((*ty).clone(), func_op, argslist),
                );
                s.push(tmp);
                return (*ty, Operand::Id(Uid(sym)), s);
            }
            panic!("Invalid Call Id in call {exp}")
        }
    }
}

fn cmp_lhs(c: &Ctxt, exp: &ast::Node<ast::Exp>) -> (llvm::Ty, llvm::Operand, Stream) {
    match exp.elt.as_ref() {
        ast::Exp::Id(id) => {
            let (t, o) = c.get(&llvm::Gid(id.0.clone())).unwrap();
            (t.to_owned(), o.to_owned(), vec![])
        }
        ast::Exp::Index(exp1, exp2) => {
            let (t1, o1, mut s1) = cmp_exp(c, exp1);
            let (_t2, o2, mut s2) = cmp_exp(c, exp2);
            let sym = gensym("");
            let mut s = vec![Elt::I(
                llvm::Uid(sym.clone()),
                llvm::Insn::Gep(
                    t1.clone(),
                    o1,
                    vec![llvm::Operand::Const(0), llvm::Operand::Const(1), o2],
                ),
            )];
            s.append(&mut s2);
            s.append(&mut s1);
            (
                llvm::Ty::Ptr(Box::new(extr_arr_t(&t1))),
                llvm::Operand::Id(llvm::Uid(sym)),
                s,
            )
        }
        _ => panic!("Invalid LHS in {exp}"),
    }
}

/* Compile a statement in context c with return typ rt. Return a new context,
  possibly extended with new local bindings, and the instruction stream
  implementing the statement.

  Left-hand-sides of assignment statements must either be OAT identifiers,
  or an index into some arbitrary expression of array type. Otherwise, the
  program is not well-formed and your compiler may throw an error.

  Tips:
  - for local variable declarations, you will need to emit Allocas in the
    entry block of the current function using the E() constructor.

  - don't forget to add a bindings to the context for local variable
    declarations

  - you can avoid some work by translating For loops to the corresponding
    While loop, building the AST and recursively calling cmp_stmt

  - you might find it helpful to reuse the code you wrote for the Call
    expression to implement the SCall statement

  - compiling the left-hand-side of an assignment is almost exactly like
    compiling the Id or Index expression. Instead of loading the resulting
    pointer, you just need to store to it!

*/

fn cmp_stmt(c: &mut Ctxt, rt: &llvm::Ty, stmt: &ast::Node<ast::Stmt>) -> Stream {
    use llvm::*;
    match (*stmt.elt).clone() {
        ast::Stmt::Ret(Some(e)) => {
            let (t, o, mut s) = cmp_exp(c, &e);
            s.push(Elt::T(Terminator::Ret(t, Some(o))));
            s
        }
        ast::Stmt::Ret(None) => vec![Elt::T(Terminator::Ret(rt.clone(), None))],
        ast::Stmt::Decl(ast::Vdecl(id, exp)) => {
            let (t, o, mut s) = cmp_exp(c, &exp);
            let sym = Uid(gensym(id.0.as_str()));
            c.insert(
                llvm::Gid(id.0),
                (Ty::Ptr(Box::new(t.clone())), Operand::Id(sym.clone())),
            );
            s.append(&mut vec![
                Elt::E(sym.clone(), Insn::Alloca(t.clone())),
                Elt::I(sym.clone(), Insn::Store(t, o, Operand::Id(sym))),
            ]);
            s
        }
        ast::Stmt::If(cond, this, that) => {
            let (_t, o, mut s) = cmp_exp(c, &cond);
            let mut s1 = cmp_block(c, rt, &this);
            let mut s2 = cmp_block(c, rt, &that);
            let (sym_this, sym_that, sym_end) = (
                llvm::Lbl(gensym("then")),
                llvm::Lbl(gensym("else")),
                llvm::Lbl(gensym("end")),
            );
            s.append(&mut vec![
                Elt::T(llvm::Terminator::Cbr(o, sym_this.clone(), sym_that.clone())),
                Elt::L(sym_this),
            ]);
            s.append(&mut s1);
            s.append(&mut vec![
                Elt::T(llvm::Terminator::Br(sym_end.clone())),
                Elt::L(sym_that),
            ]);
            s.append(&mut s2);
            s.push(Elt::T(llvm::Terminator::Br(sym_end.clone())));
            s.push(Elt::L(sym_end));
            s
        }
        ast::Stmt::While(cond, body) => {
            let (_t, o, mut s) = cmp_exp(c, &cond);
            let mut body_ll = cmp_block(c, rt, &body);
            let (sym_pre, sym_body, sym_post) = (
                llvm::Lbl(gensym("pre")),
                llvm::Lbl(gensym("body")),
                llvm::Lbl(gensym("post")),
            );
            let mut ss = vec![
                Elt::T(llvm::Terminator::Br(sym_pre.clone())),
                Elt::L(sym_pre.clone()),
            ];
            ss.append(&mut s);
            ss.push(Elt::T(llvm::Terminator::Cbr(
                o,
                sym_body.clone(),
                sym_post.clone(),
            )));
            ss.push(Elt::L(sym_body));
            ss.append(&mut body_ll);
            ss.push(Elt::T(llvm::Terminator::Br(sym_pre)));
            ss.push(Elt::L(sym_post));
            ss
        }
        ast::Stmt::For(init, cond, expr, body) => {
            let mut decls = init.into_iter().fold(vec![], |mut acc, v| {
                let mut str = cmp_stmt(c, rt, &ast::Node::no_loc(Box::new(ast::Stmt::Decl(v))));
                acc.append(&mut str);
                acc
            });

            let mut while_body = body;
            if let Some(e) = expr {
                while_body.0.append(&mut vec![e]);
            }
            let while_cond = if let Some(e) = cond {
                e
            } else {
                ast::Node::no_loc(Box::new(ast::Exp::CBool(true)))
            };
            let mut str = cmp_stmt(
                c,
                rt,
                &ast::Node::no_loc(Box::new(ast::Stmt::While(while_cond, while_body))),
            );
            decls.append(&mut str);
            decls
        }
        ast::Stmt::SCall(call) => {
            let (_t, _op, stream) = cmp_exp(c, &ast::Node::no_loc(Box::new(ast::Exp::Call(call))));
            stream
        }
        ast::Stmt::Assn(lhs, rhs) => {
            let (_tl, ol, mut sl) = cmp_lhs(c, &lhs);
            let (tr, or, mut sr) = cmp_exp(c, &rhs);
            sl.append(&mut sr);
            sl.push(Elt::I(
                llvm::Uid("".to_owned()),
                llvm::Insn::Store(tr, or, ol),
            ));
            sl
        }
    }
}

// Compile a series of statements
fn cmp_block(c: &mut Ctxt, rt: &llvm::Ty, stmts: &ast::Block) -> Stream {
    stmts.0.clone().into_iter().fold(vec![], |mut code, s| {
        let mut stmt_code = cmp_stmt(c, rt, &s);
        code.append(&mut stmt_code);
        code
    })
}

/*Adds each function identifer to the context at an
appropriately translated type.

NOTE: The Gid of a function is just its source name */

fn cmp_function_context(c: &mut Ctxt, p: &ast::Prog) {
    p.0.clone().into_iter().for_each(|f| match f {
        ast::Decl::Gfdecl(ast::Node { elt, loc: _ }) => {
            let ft = ast::Ty::Ref(Box::new(ast::Rty::Fun(
                elt.args.into_iter().map(|a| a.0).collect(),
                elt.frtyp,
            )));
            c.insert(
                llvm::Gid(elt.fname.0.clone()),
                (cmp_ty(&ft), llvm::Operand::Gid(llvm::Gid(elt.fname.0))),
            );
        }
        ast::Decl::Gvdecl(_) => {}
    });
}

/*Populate a context with bindings for global variables
mapping OAT identifiers to LLVMlite gids and their types.

Only a small subset of OAT expressions can be used as global initializers
in well-formed programs. (The constructors starting with C). */

fn cmp_global_ctxt(c: &mut Ctxt, p: &ast::Prog) {
    p.0.clone().into_iter().for_each(|f| match f {
        ast::Decl::Gvdecl(ast::Node { elt, loc: _ }) => {
            let init = *(*elt.init).elt;
            let t = match init {
                ast::Exp::CBool(_) => ast::Ty::Bool,
                ast::Exp::CNull(t) => ast::Ty::Ref(Box::new(t)),
                ast::Exp::CInt(_) => ast::Ty::Int,
                ast::Exp::CArr(t, _) => ast::Ty::Ref(Box::new(ast::Rty::Array(t))),
                ast::Exp::CStr(_) => ast::Ty::Ref(Box::new(ast::Rty::String)),
                _ => panic!("Invalid global initializer"),
            };
            c.insert(
                llvm::Gid(elt.name.0.clone()),
                (
                    llvm::Ty::Ptr(Box::new(cmp_ty(&t))),
                    llvm::Operand::Gid(llvm::Gid(elt.name.0)),
                ),
            );
        }
        ast::Decl::Gfdecl(_) => (),
    });
}

/*
Compile a function declaration in global context c. Return the LLVMlite cfg
   and a list of global declarations containing the string literals appearing
   in the function.

   You will need to
   1. Allocate stack space for the function parameters using Alloca
   2. Store the function arguments in their corresponding alloca'd stack slot
   3. Extend the context with bindings for function variables
   4. Compile the body of the function using cmp_block
   5. Use cfg_of_stream to produce a LLVMlite cfg from
*/

fn cmp_fdecl(
    c: &mut Ctxt,
    f: &ast::Node<ast::Fdecl>,
) -> (llvm::Fdecl, Vec<(ll::ll::Gid, ll::ll::Gdecl)>) {
    let mut param_types: Vec<ast::Ty> = vec![];
    let mut param_ids: Vec<llvm::Uid> = vec![];
    let mut s: Vec<Elt> = vec![];
    f.elt
        .args
        .clone()
        .into_iter()
        .for_each(|ast::Arg(typ, id)| {
            let sym = llvm::Uid(gensym(id.0.as_str()));
            let llty = cmp_ty(&typ);
            c.insert(
                llvm::Gid(id.clone().0),
                (
                    llvm::Ty::Ptr(Box::new(llty.clone())),
                    llvm::Operand::Id(sym.clone()),
                ),
            );
            let mut new_st = vec![
                Elt::E(sym.clone(), llvm::Insn::Alloca(llty.clone())),
                Elt::I(
                    sym.clone(),
                    llvm::Insn::Store(
                        llty,
                        llvm::Operand::Id(llvm::Uid(id.clone().0)),
                        llvm::Operand::Id(sym),
                    ),
                ),
            ];
            s.append(&mut new_st);
            param_types.push(typ);
            param_ids.push(llvm::Uid(id.0));
        });

    let mut s2 = cmp_block(c, &cmp_ret_ty(&f.elt.frtyp), &f.elt.body);
    s.append(&mut s2);

    let (cfg, globals) = cfg_of_stream(s);
    (
        llvm::Fdecl {
            f_ty: cmp_fty(&param_types, &f.elt.frtyp),
            f_param: param_ids,
            f_cfg: cfg,
        },
        globals,
    )
}

/* Compile a global initializer, returning the resulting LLVMlite global
declaration, and a list of additional global declarations.

Tips:
- Only CNull, CBool, CInt, CStr, and CArr can appear as global initializers
  in well-formed OAT programs. Your compiler may throw an error for the other
  cases

- OAT arrays are always handled via pointers. A global array of arrays will
  be an array of pointers to arrays emitted as additional global declarations.
  */

fn cmp_gexp(
    c: &mut Ctxt,
    e: &ast::Node<ast::Exp>,
) -> (llvm::Gdecl, Vec<(ll::ll::Gid, ll::ll::Gdecl)>) {
    use llvm::*;
    match (*e.elt).clone() {
        ast::Exp::CNull(t) => (
            llvm::Gdecl(cmp_ty(&ast::Ty::Ref(Box::new(t))), Ginit::GNull),
            vec![],
        ),
        ast::Exp::CInt(i) => (Gdecl(Ty::I64, Ginit::GInt(i)), vec![]),
        ast::Exp::CBool(b) => (Gdecl(Ty::I1, Ginit::GInt(if b { 1 } else { 0 })), vec![]),
        ast::Exp::CStr(s) => {
            let sym = Gid(gensym("gid"));
            let str_t = Ty::Array(s.len() as i64 + 1, Box::new(Ty::I8));
            let target_decl = Gdecl(
                Ty::Ptr(Box::new(Ty::I8)),
                Ginit::GBitcast(
                    Ty::Ptr(Box::new(str_t.clone())),
                    Box::new(Ginit::GGid(sym.clone())),
                    Ty::Ptr(Box::new(Ty::I8)),
                ),
            );
            (target_decl, vec![(sym, Gdecl(str_t, Ginit::GString(s)))])
        }
        ast::Exp::CArr(t, exps) => {
            let exp_cmp = exps
                .clone()
                .into_iter()
                .map(|e| cmp_gexp(c, &e))
                .collect::<Vec<_>>();
            let arr_size = exps.len() as i64;
            let inside_t = cmp_ty(&t);
            let oat_arr_t = cmp_ty(&ast::Ty::Ref(Box::new(ast::Rty::Array(t.clone()))));
            let ll_arr_t = Ty::Struct(vec![
                Box::new(Ty::I64),
                Box::new(Ty::Array(arr_size, Box::new(inside_t.clone()))),
            ]);
            let ll_t = Ty::Array(arr_size, Box::new(cmp_ty(&t)));
            let (exp_cmp2, mut hdecls) = match t {
                ast::Ty::Ref(t) if matches!(*t, ast::Rty::Array(_)) => {
                    if let ast::Rty::Array(_ti) = *t {
                        let inner_t = Ty::Ptr(Box::new(exp_cmp[0].clone().0 .0));
                        let gids: Vec<Gid> = (0..arr_size)
                            .map(|i| Gid(gensym(format!("gid_{}", i).as_str())))
                            .collect();
                        let hdecls = gids
                            .clone()
                            .into_iter()
                            .zip(exp_cmp)
                            .flat_map(|(gid, (exp, mut hdecls))| {
                                hdecls.push((gid, exp));
                                hdecls
                            })
                            .collect::<Vec<_>>();
                        let exp_cmp = gids
                            .into_iter()
                            .map(|gid| {
                                Gdecl(
                                    inside_t.clone(),
                                    Ginit::GBitcast(
                                        inner_t.clone(),
                                        Box::new(Ginit::GGid(gid)),
                                        inside_t.clone(),
                                    ),
                                )
                            })
                            .collect::<Vec<_>>();
                        (exp_cmp, hdecls)
                    } else {
                        panic!("unexpected array type");
                    }
                }
                _ => (
                    exp_cmp.into_iter().map(|decl| decl.0).collect::<Vec<_>>(),
                    vec![],
                ),
            };
            let sym = gensym("gid");
            let target_decl = Gdecl(
                oat_arr_t.clone(),
                Ginit::GBitcast(
                    Ty::Ptr(Box::new(ll_arr_t.clone())),
                    Box::new(Ginit::GGid(Gid(sym.clone()))),
                    oat_arr_t,
                ),
            );
            let aux_decl = (
                Gid(sym),
                Gdecl(
                    ll_arr_t,
                    Ginit::GStruct(vec![
                        Gdecl(Ty::I64, Ginit::GInt(arr_size)),
                        Gdecl(ll_t, Ginit::GArray(exp_cmp2)),
                    ]),
                ),
            );
            hdecls.insert(0, aux_decl);
            (target_decl, hdecls)
        }
        _ => panic!("Should not happen"),
    }
}

fn internals() -> HashMap<llvm::Gid, llvm::Ty> {
    HashMap::from([(
        llvm::Gid("oat_alloc_array".to_owned()),
        llvm::Ty::Fun(
            vec![llvm::Ty::I64],
            Box::new(llvm::Ty::Ptr(Box::new(llvm::Ty::I64))),
        ),
    )])
}

fn builtins() -> HashMap<llvm::Gid, llvm::Ty> {
    HashMap::from([
        (
            llvm::Gid("array_of_string".to_owned()),
            cmp_rty(&ast::Rty::Fun(
                vec![ast::Ty::Ref(Box::new(ast::Rty::String))],
                ast::RetTy::Val(ast::Ty::Ref(Box::new(ast::Rty::Array(ast::Ty::Int)))),
            )),
        ),
        (
            llvm::Gid("string_of_array".to_owned()),
            cmp_rty(&ast::Rty::Fun(
                vec![ast::Ty::Ref(Box::new(ast::Rty::Array(ast::Ty::Int)))],
                ast::RetTy::Val(ast::Ty::Ref(Box::new(ast::Rty::String))),
            )),
        ),
        (
            llvm::Gid("length_of_string".to_owned()),
            cmp_rty(&ast::Rty::Fun(
                vec![ast::Ty::Ref(Box::new(ast::Rty::String))],
                ast::RetTy::Val(ast::Ty::Int),
            )),
        ),
        (
            llvm::Gid("string_of_int".to_owned()),
            cmp_rty(&ast::Rty::Fun(
                vec![ast::Ty::Int],
                ast::RetTy::Val(ast::Ty::Ref(Box::new(ast::Rty::String))),
            )),
        ),
        (
            llvm::Gid("string_cat".to_owned()),
            cmp_rty(&ast::Rty::Fun(
                vec![
                    ast::Ty::Ref(Box::new(ast::Rty::String)),
                    ast::Ty::Ref(Box::new(ast::Rty::String)),
                ],
                ast::RetTy::Val(ast::Ty::Ref(Box::new(ast::Rty::String))),
            )),
        ),
        (
            llvm::Gid("print_string".to_owned()),
            cmp_rty(&ast::Rty::Fun(
                vec![ast::Ty::Ref(Box::new(ast::Rty::String))],
                ast::RetTy::Void,
            )),
        ),
        (
            llvm::Gid("print_int".to_owned()),
            cmp_rty(&ast::Rty::Fun(vec![ast::Ty::Int], ast::RetTy::Void)),
        ),
        (
            llvm::Gid("print_bool".to_owned()),
            cmp_rty(&ast::Rty::Fun(vec![ast::Ty::Bool], ast::RetTy::Void)),
        ),
    ])
}

// Compile a OAT program to LLVMlite
pub fn cmp_prog(p: ast::Prog) -> llvm::Prog {
    // add built-in functions to context
    let mut init_ctxt = builtins()
        .iter()
        .fold(HashMap::new(), |mut ctxt, (name, ty)| {
            ctxt.insert(
                llvm::Gid(name.to_string()),
                (
                    llvm::Ty::Ptr(Box::new(ty.clone())),
                    llvm::Operand::Gid(llvm::Gid(name.to_string())),
                ),
            );
            ctxt
        });

    cmp_function_context(&mut init_ctxt, &p);
    cmp_global_ctxt(&mut init_ctxt, &p);

    let mut fdecls_: Vec<(llvm::Gid, llvm::Fdecl)> = Vec::new();
    let mut gdecls_: Vec<(llvm::Gid, llvm::Gdecl)> = Vec::new();

    p.0.into_iter().rev().for_each(|d| match d {
        ast::Decl::Gvdecl(ast::Node { elt: gd, .. }) => {
            let (ll_gd, mut gs_) = cmp_gexp(&mut init_ctxt, gd.init.as_ref());

            gdecls_.append(&mut gs_);
            gdecls_.push((llvm::Gid(gd.name.0), ll_gd));
        }
        ast::Decl::Gfdecl(fd) => {
            let (fdecl, mut gs_) = cmp_fdecl(&mut init_ctxt, &fd);
            gdecls_.append(&mut gs_);
            fdecls_.push((llvm::Gid(fd.elt.fname.0), fdecl));
        }
    });

    let mut fdecls: HashMap<llvm::Gid, llvm::Fdecl> = HashMap::new();
    for (gid, fdecl) in fdecls_ {
        fdecls.insert(gid, fdecl);
    }

    let mut gdecls: HashMap<llvm::Gid, llvm::Gdecl> = HashMap::new();
    for (gid, gdecl) in gdecls_ {
        gdecls.insert(gid, gdecl);
    }

    let mut edecls = internals();
    edecls.extend(builtins());

    llvm::Prog {
        tdecls: HashMap::new(),
        gdecls,
        fdecls,
        edecls,
    }
}
