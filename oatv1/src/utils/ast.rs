use crate::ast::{self, Exp, Node, Stmt};

// TODO all pretty-print functions have been skipped

pub fn prec_of_binop(bop: &ast::Binop) -> u32 {
    match bop {
        ast::Binop::Mul => 100,
        ast::Binop::Add | ast::Binop::Sub => 90,
        ast::Binop::Shl | ast::Binop::Shr | ast::Binop::Sar => 80,
        ast::Binop::Lt | ast::Binop::Lte | ast::Binop::Gt | ast::Binop::Gte => 70,
        ast::Binop::Eq | ast::Binop::Neq => 60,
        ast::Binop::And => 50,
        ast::Binop::Or => 40,
        ast::Binop::IAnd => 30,
        ast::Binop::IOr => 20,
    }
}

pub fn prec_of_unnop(_: &ast::Unop) -> u32 {
    110
}

pub fn prec_of_exp(e: &ast::Exp) -> u32 {
    match e {
        ast::Exp::Bop(bop, _, _) => prec_of_binop(bop),
        ast::Exp::Uop(unop, _) => prec_of_unnop(unop),
        _ => 0,
    }
}

fn to_noloc<T: ast::NoLocable + std::fmt::Display>(n: &Node<T>) -> Node<T> {
    Node::no_loc(Box::new(n.elt.as_ref().reset_nodes()))
}

impl ast::NoLocable for ast::Exp {
    fn reset_nodes(&self) -> Self {
        return match self {
            Exp::CNull(_) | Exp::CBool(_) | Exp::CInt(_) | Exp::CStr(_) | Exp::Id(_) => {
                self.clone()
            }
            Exp::CArr(t, ns) => Exp::CArr(t.clone(), ns.iter().map(to_noloc).collect()),
            Exp::NewArr(t, n) => Exp::NewArr(t.clone(), to_noloc(n)),
            Exp::Index(n, n1) => Exp::Index(to_noloc(n), to_noloc(n1)),
            Exp::Call(c) => Exp::Call(c.reset_nodes()),
            Exp::Bop(b, n, n1) => Exp::Bop(b.clone(), to_noloc(n), to_noloc(n1)),
            Exp::Uop(u, n) => Exp::Uop(u.clone(), to_noloc(n)),
        };
    }
}

impl ast::NoLocable for ast::Stmt {
    fn reset_nodes(&self) -> Self {
        return match self {
            Stmt::Assn(n1, n2) => Stmt::Assn(to_noloc(n1), to_noloc(n2)),
            Stmt::Decl(n) => Stmt::Decl(n.reset_nodes()),
            Stmt::Ret(opt) => Stmt::Ret(opt.as_ref().map(to_noloc)),
            Stmt::SCall(c) => Stmt::SCall(c.reset_nodes()),
            Stmt::If(n, s1, s2) => Stmt::If(to_noloc(n), s1.reset_nodes(), s2.reset_nodes()),
            Stmt::For(d, e, s, b) => Stmt::For(
                d.iter().map(|x| x.reset_nodes()).collect(),
                e.as_ref().map(to_noloc),
                s.as_ref().map(to_noloc),
                b.reset_nodes(),
            ),
            Stmt::While(n, s) => Stmt::While(to_noloc(n), s.reset_nodes()),
        };
    }
}

impl ast::NoLocable for ast::Vdecl {
    fn reset_nodes(&self) -> Self {
        ast::Vdecl(self.0.clone(), to_noloc(&self.1))
    }
}

impl ast::NoLocable for ast::Block {
    fn reset_nodes(&self) -> Self {
        ast::Block(self.0.iter().map(to_noloc).collect())
    }
}
impl ast::NoLocable for ast::CallTy {
    fn reset_nodes(&self) -> Self {
        ast::CallTy(to_noloc(&self.0), self.1.iter().map(to_noloc).collect())
    }
}

impl ast::NoLocable for ast::Gdecl {
    fn reset_nodes(&self) -> Self {
        ast::Gdecl {
            name: self.name.clone(),
            init: Box::new(to_noloc(&self.init)),
        }
    }
}

impl ast::NoLocable for ast::Fdecl {
    fn reset_nodes(&self) -> Self {
        ast::Fdecl {
            frtyp: self.frtyp.clone(),
            fname: self.fname.clone(),
            args: self.args.clone(),
            body: self.body.reset_nodes(),
        }
    }
}

impl ast::NoLocable for ast::Decl {
    fn reset_nodes(&self) -> Self {
        match self {
            ast::Decl::Gvdecl(d) => ast::Decl::Gvdecl(to_noloc(d)),
            ast::Decl::Gfdecl(g) => ast::Decl::Gfdecl(to_noloc(g)),
        }
    }
}
impl ast::NoLocable for ast::Prog {
    fn reset_nodes(&self) -> Self {
        ast::Prog(self.0.iter().map(|x| x.to_owned().reset_nodes()).collect())
    }
}
