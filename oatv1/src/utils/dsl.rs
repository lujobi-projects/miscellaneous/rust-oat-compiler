#[macro_export]
macro_rules! no_loc {
    ($x:expr) => {
        Node::no_loc(Box::new($x))
    };
}
#[macro_export]
macro_rules! no_loc_box {
    ($x:expr) => {
        Box::new(Node::no_loc(Box::new($x)))
    };
}
#[macro_export]
macro_rules! no_loc_vec {
    ($($args:expr),*) => {
        vec![$(no_loc!($args), )*]
    };
}
#[macro_export]
macro_rules! oat_id {
    ($x:expr) => {
        ast::Id(stringify!($x).to_string())
    };
}
#[macro_export]
macro_rules! oat_id_exp {
    ($x:expr) => {
        Exp::Id(oat_id!($x))
    };
}
#[macro_export]
macro_rules! decl {
    ($x:expr, $y:expr) => {
        ast::Stmt::Decl(ast::Vdecl(oat_id!($x), no_loc!($y)))
    };
}
#[macro_export]
macro_rules! node {
    ($x:expr, $y:expr) => {
        ast::Node {
            elt: Box::new($x),
            loc: $y,
        }
    };
}

#[macro_export]
macro_rules! gvdecl {
    ($name:expr, $init:expr) => {
        ast::Decl::Gvdecl(no_loc!(ast::Gdecl {
            name: oat_id!($name),
            init: no_loc_box!($init),
        }))
    };
}

#[macro_export]
macro_rules! gfdecl {
    ($frtyp:expr, $fname:expr, $args:expr, $body:expr) => {
        Decl::Gfdecl(no_loc!(Fdecl {
            frtyp: $frtyp,
            fname: oat_id!($fname),
            args: $args,
            body: Block($body)
        }))
    };
}

#[macro_export]
macro_rules! bop {
    ($op:expr, $x:expr, $y:expr) => {
        ast::Exp::Bop($op, no_loc!($x), no_loc!($y))
    };
}

#[macro_export]
macro_rules! uop {
    ($op:expr, $x:expr) => {
        ast::Exp::Uop($op, no_loc!($x))
    };
}

#[macro_export]
macro_rules! index {
    ($x:expr, $y:expr) => {
        ast::Exp::Index(no_loc!($x), no_loc!($y))
    };
}

#[macro_export]
macro_rules! assn {
    ($x:expr, $y:expr) => {
        ast::Stmt::Assn(no_loc!($x), no_loc!($y))
    };
}
#[macro_export]
macro_rules! cstr {
    ($x:expr) => {
        ast::Exp::CStr(stringify!($x).to_string())
    };
}
#[macro_export]
macro_rules! oat_ref {
    ($x:expr) => {
        Ty::Ref(Box::new($x))
    };
}
#[macro_export]
macro_rules! ptr_ty {
    ($x:expr) => {
        Ty::Ptr(Box::new($x))
    };
}

#[macro_export]
macro_rules! range {
    ($x:expr, $l0:expr, $l1:expr, $l2:expr, $l3:expr) => {
        oatv1::utils::range::Range(
            $x.to_string(),
            oatv1::utils::location::Location::new($l0, $l1),
            oatv1::utils::location::Location::new($l2, $l3),
        )
    };
}
