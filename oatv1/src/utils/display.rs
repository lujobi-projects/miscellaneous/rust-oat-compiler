use crate::ast::{self};

// Display helpers
fn string_of_list<T>(list: &[T]) -> String
where
    T: std::fmt::Display,
{
    let mut s = String::from("[ ");
    for (i, t) in list.iter().enumerate() {
        if i > 0 {
            s.push_str(" ; ");
        }
        s.push_str(&t.to_string());
    }
    s.push_str(" ]");
    s
}

fn string_of_option<T>(opt: &Option<T>) -> String
where
    T: std::fmt::Display,
{
    match opt {
        Some(t) => format!("Some ({})", t),
        None => String::from("None"),
    }
}

// Display functions for the AST

impl<T: std::fmt::Display> std::fmt::Display for ast::Node<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{{ elt = {}; loc = {} }}", self.elt, self.loc)
    }
}
impl std::fmt::Display for ast::Ty {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ast::Ty::Bool => write!(f, "tBool"),
            ast::Ty::Int => write!(f, "Tint"),
            ast::Ty::Ref(ty) => write!(f, "TRef {}", *ty),
        }
    }
}

impl std::fmt::Display for ast::RetTy {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ast::RetTy::Void => write!(f, "TVoid"),
            ast::RetTy::Val(ty) => write!(f, "RetVal {}", *ty),
        }
    }
}

impl std::fmt::Display for ast::Rty {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ast::Rty::String => write!(f, "RString"),
            ast::Rty::Array(ty) => write!(f, "(RArray ({}))", *ty),
            ast::Rty::Fun(args, ret) => write!(f, "RFun({}, {})", string_of_list(args), ret),
        }
    }
}

impl std::fmt::Display for ast::Id {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "\"{}\"", self.0)
    }
}

impl std::fmt::Display for ast::Binop {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ast::Binop::Add => write!(f, "+"),
            ast::Binop::Sub => write!(f, "-"),
            ast::Binop::Mul => write!(f, "*"),
            ast::Binop::Eq => write!(f, "=="),
            ast::Binop::Neq => write!(f, "!="),
            ast::Binop::Lt => write!(f, "<"),
            ast::Binop::Lte => write!(f, "<="),
            ast::Binop::Gt => write!(f, ">"),
            ast::Binop::Gte => write!(f, ">="),
            ast::Binop::And => write!(f, "&"),
            ast::Binop::Or => write!(f, "|"),
            ast::Binop::IAnd => write!(f, "[&]"),
            ast::Binop::IOr => write!(f, "[|]"),
            ast::Binop::Shl => write!(f, "<<"),
            ast::Binop::Shr => write!(f, ">>"),
            ast::Binop::Sar => write!(f, ">>>"),
        }
    }
}

impl std::fmt::Display for ast::Unop {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ast::Unop::Neg => write!(f, "Neg"),
            ast::Unop::Lognot => write!(f, "Lognot"),
            ast::Unop::Bitnot => write!(f, "Bitnot"),
        }
    }
}

impl std::fmt::Display for ast::Exp {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ast::Exp::CNull(ty) => write!(f, "CNull {}", ty),
            ast::Exp::CBool(b) => write!(f, "CBool {}", b),
            ast::Exp::CInt(i) => write!(f, "CInt {}", i),
            ast::Exp::CStr(s) => write!(f, "CStr {}", s),
            ast::Exp::CArr(t, cs) => write!(f, "CArr {} {}", t, string_of_list(cs)),
            ast::Exp::Id(id) => write!(f, "Id {}", id),
            ast::Exp::Index(e, i) => write!(f, "Index ({} {})", e, i),
            ast::Exp::Call(c) => write!(f, "Call ({})", c),
            ast::Exp::NewArr(t, e) => write!(f, "NewArr ({},{})", t, e),
            ast::Exp::Bop(b, e1, e2) => write!(f, "Bop ({},{},{})", b, e1, e2),
            ast::Exp::Uop(u, e) => write!(f, "Uop ({},{})", u, e),
        }
    }
}

impl std::fmt::Display for ast::CallTy {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Call ({} {})", self.0, string_of_list(&self.1))
    }
}

impl std::fmt::Display for ast::Cfield {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} = {};", self.0, self.1)
    }
}

impl std::fmt::Display for ast::Vdecl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.0, self.1)
    }
}

impl std::fmt::Display for ast::Stmt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ast::Stmt::Assn(e1, e2) => write!(f, "Assn ({},{})", e1, e2),
            ast::Stmt::Decl(d) => write!(f, "Decl ({})", d),
            ast::Stmt::Ret(e) => write!(f, "Ret ({})", string_of_option(e)),
            ast::Stmt::SCall(c) => write!(f, "SCall ({})", c),
            ast::Stmt::If(e, s1, s2) => write!(f, "If ({},{},{})", e, s1, s2),
            ast::Stmt::For(d, e, s, b) => {
                write!(
                    f,
                    "For ({},{},{},{})",
                    string_of_list(d),
                    string_of_option(e),
                    string_of_option(s),
                    b
                )
            }
            ast::Stmt::While(e, s) => write!(f, "While ({},{})", e, s),
        }
    }
}

impl std::fmt::Display for ast::Block {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", string_of_list(&self.0))
    }
}

impl std::fmt::Display for ast::Arg {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.0, self.1)
    }
}

impl std::fmt::Display for ast::Fdecl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{{ frtyp = {}, fname = {}, args = {}, body = {} }}",
            self.frtyp,
            self.fname,
            string_of_list(&self.args),
            self.body
        )
    }
}

impl std::fmt::Display for ast::Gdecl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{{ name = {}; init = {} }}", self.name, self.init)
    }
}

impl std::fmt::Display for ast::Decl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ast::Decl::Gvdecl(d) => write!(f, "Gvdecl ({})", d),
            ast::Decl::Gfdecl(g) => write!(f, "Gfdecl ({})", g),
        }
    }
}

impl std::fmt::Display for ast::Prog {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", string_of_list(&self.0))
    }
}
