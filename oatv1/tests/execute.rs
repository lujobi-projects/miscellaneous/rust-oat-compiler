mod commons;

mod execute {
    use utils::{
        current_dir,
        driver::{self, write_file},
        platform,
    };

    const OUTPUT_PATH: &str = "oat_output";
    use crate::{assert_res_equal, commons::*};

    use oatv1::frontend;

    fn oat_file_test(path: &str, args: Vec<&str>) -> anyhow::Result<String> {
        println!("** Processing: {}", path);

        let dot_ll_file = platform::gen_name(&current_dir!(OUTPUT_PATH), "test", ".ll")?;
        let exec_file = platform::gen_name(&current_dir!(OUTPUT_PATH), "exec", "")?;

        let oat_ast = parse_file(&current_dir!(path))?;
        let ll_ast = frontend::cmp_prog(oat_ast);
        let ll_str = ll_ast.to_string();
        write_file(&dot_ll_file, &ll_str)?;

        platform::link(vec![&dot_ll_file, &current_dir!("runtime.c")], &exec_file)?;
        let result = driver::run_program(args, exec_file.as_str())?;
        println!(
            "** Executable output (code: {}):\n{} {}",
            result.0, result.1, result.2
        );
        driver::remove_file(&dot_ll_file)?;
        driver::remove_file(&exec_file)?;
        Ok(format!("{}{}{}", result.1, result.2, result.0))
    }

    #[test]
    fn easiest() {
        assert_res_equal!(oat_file_test("oatprograms/easyrun1.oat", vec![]), "17");
        assert_res_equal!(oat_file_test("oatprograms/easyrun2.oat", vec![]), "35");
        assert_res_equal!(oat_file_test("oatprograms/easyrun3.oat", vec![]), "73");
        assert_res_equal!(oat_file_test("oatprograms/easyrun4.oat", vec![]), "6");
        assert_res_equal!(oat_file_test("oatprograms/easyrun5.oat", vec![]), "212");
        assert_res_equal!(oat_file_test("oatprograms/easyrun6.oat", vec![]), "9");
        assert_res_equal!(oat_file_test("oatprograms/easyrun7.oat", vec![]), "23");
        assert_res_equal!(oat_file_test("oatprograms/easyrun8.oat", vec![]), "160");
        assert_res_equal!(oat_file_test("oatprograms/easyrun9.oat", vec![]), "236");
        assert_res_equal!(oat_file_test("oatprograms/easyrun10.oat", vec![]), "254");
    }

    #[test]
    fn globals() {
        assert_res_equal!(oat_file_test("oatprograms/globals1.oat", vec![]), "42");
        assert_res_equal!(oat_file_test("oatprograms/globals2.oat", vec![]), "17");
        assert_res_equal!(oat_file_test("oatprograms/globals3.oat", vec![]), "17");
        assert_res_equal!(oat_file_test("oatprograms/globals4.oat", vec![]), "5");
        assert_res_equal!(oat_file_test("oatprograms/globals5.oat", vec![]), "17");
        assert_res_equal!(oat_file_test("oatprograms/globals6.oat", vec![]), "15");
        assert_res_equal!(oat_file_test("oatprograms/globals7.oat", vec![]), "3");
    }

    #[test]
    fn path() {
        assert_res_equal!(oat_file_test("oatprograms/path1.oat", vec![]), "17");
        assert_res_equal!(oat_file_test("oatprograms/path2.oat", vec![]), "35");
        assert_res_equal!(oat_file_test("oatprograms/path3.oat", vec![]), "3");
        assert_res_equal!(oat_file_test("oatprograms/arrayargs.oat", vec![]), "17");
        assert_res_equal!(oat_file_test("oatprograms/arrayargs1.oat", vec![]), "17");
        assert_res_equal!(oat_file_test("oatprograms/arrayargs2.oat", vec![]), "17");
        assert_res_equal!(oat_file_test("oatprograms/arrayargs3.oat", vec![]), "34");
    }

    #[test]
    fn easy() {
        assert_res_equal!(oat_file_test("oatprograms/fact.oat", vec![]), "1200");
        assert_res_equal!(oat_file_test("oatprograms/run1.oat", vec![]), "153");
        assert_res_equal!(oat_file_test("oatprograms/run2.oat", vec![]), "6");
        assert_res_equal!(oat_file_test("oatprograms/run3.oat", vec![]), "2");
        assert_res_equal!(oat_file_test("oatprograms/run4.oat", vec![]), "42");
        assert_res_equal!(oat_file_test("oatprograms/run5.oat", vec![]), "4");
        assert_res_equal!(oat_file_test("oatprograms/run6.oat", vec![]), "1");
        assert_res_equal!(oat_file_test("oatprograms/run7.oat", vec![]), "20");
        assert_res_equal!(oat_file_test("oatprograms/run8.oat", vec![]), "2");
        assert_res_equal!(oat_file_test("oatprograms/run9.oat", vec![]), "4");
        assert_res_equal!(oat_file_test("oatprograms/run10.oat", vec![]), "5");
        assert_res_equal!(oat_file_test("oatprograms/run11.oat", vec![]), "7");
        assert_res_equal!(oat_file_test("oatprograms/run14.oat", vec![]), "16");
        assert_res_equal!(oat_file_test("oatprograms/run15.oat", vec![]), "19");
        assert_res_equal!(oat_file_test("oatprograms/run16.oat", vec![]), "13");
        assert_res_equal!(oat_file_test("oatprograms/run18.oat", vec![]), "231");
        assert_res_equal!(oat_file_test("oatprograms/run19.oat", vec![]), "231");
        assert_res_equal!(oat_file_test("oatprograms/run20.oat", vec![]), "19");
        assert_res_equal!(oat_file_test("oatprograms/run22.oat", vec![]), "abc0");
        assert_res_equal!(oat_file_test("oatprograms/run23.oat", vec![]), "1230");
        assert_res_equal!(oat_file_test("oatprograms/run24.oat", vec![]), "0");
        assert_res_equal!(oat_file_test("oatprograms/run25.oat", vec![]), "nnn0");
        assert_res_equal!(oat_file_test("oatprograms/run43.oat", vec![]), "42");
        assert_res_equal!(oat_file_test("oatprograms/run44.oat", vec![]), "hello0");
        assert_res_equal!(oat_file_test("oatprograms/run45.oat", vec![]), "420");
        assert_res_equal!(oat_file_test("oatprograms/run46.oat", vec![]), "420");
        assert_res_equal!(oat_file_test("oatprograms/run47.oat", vec![]), "3");
        assert_res_equal!(oat_file_test("oatprograms/run48.oat", vec![]), "11");
        assert_res_equal!(oat_file_test("oatprograms/run53.oat", vec![]), "nnn0");
        assert_res_equal!(oat_file_test("oatprograms/lib4.oat", vec![]), "53220");
        assert_res_equal!(oat_file_test("oatprograms/lib5.oat", vec![]), "20");
        assert_res_equal!(oat_file_test("oatprograms/lib6.oat", vec![]), "56553");
        assert_res_equal!(oat_file_test("oatprograms/lib7.oat", vec![]), "53");
        assert_res_equal!(
            oat_file_test("oatprograms/lib8.oat", vec![]),
            "Hello world!0"
        );
        assert_res_equal!(
            oat_file_test("oatprograms/lib9.oat", vec!["a", "b", "c", "d"]),
            "abcd5"
        );
        assert_res_equal!(oat_file_test("oatprograms/lib11.oat", vec![]), "45");
        assert_res_equal!(
            oat_file_test("oatprograms/lib14.oat", vec![]),
            "~}|{zyxwvu0"
        );
        assert_res_equal!(
            oat_file_test("oatprograms/lib15.oat", vec!["123456789"]),
            "456780"
        );
    }

    #[test]
    fn hard() {
        assert_res_equal!(oat_file_test("oatprograms/fac.oat", vec![]), "120");
        assert_res_equal!(
            oat_file_test("oatprograms/qsort.oat", vec![]),
            "kpyf{shomfhkmopsy{255"
        );
        assert_res_equal!(
            oat_file_test("oatprograms/bsort.oat", vec![]),
            "y}xotnuw notuwxy}255"
        );
        assert_res_equal!(
            oat_file_test("oatprograms/msort.oat", vec![]),
            "~}|{zyxwvu uvwxyz{|}~ 0"
        );
        assert_res_equal!(
            oat_file_test("oatprograms/msort2.oat", vec![]),
            "~}|{zyxwvu uvwxyz{|}~ 0"
        );
        assert_res_equal!(
            oat_file_test("oatprograms/selectionsort.oat", vec![]),
            "01253065992000"
        );
        assert_res_equal!(
            oat_file_test("oatprograms/matrixmult.oat", vec![]),
            "19 16 13 23 \t5 6 7 6 \t19 16 13 23 \t5 6 7 6 \t0"
        );
    }

    #[test]
    fn old_student_tests() {
        assert_res_equal!(
            oat_file_test("oatprograms/binary_search.oat", vec![]),
            "Correct!0"
        );
        assert_res_equal!(
            oat_file_test("oatprograms/xor_shift.oat", vec![]),
            "838867572\n22817190600"
        );
        assert_res_equal!(oat_file_test("oatprograms/sieve.oat", vec![]), "25");
        assert_res_equal!(
            oat_file_test("oatprograms/count_sort.oat", vec![]),
            "AFHZAAEYC\nAAACEFHYZ0"
        );
        assert_res_equal!(oat_file_test("oatprograms/fibo.oat", vec![]), "0");
        assert_res_equal!(oat_file_test("oatprograms/heap.oat", vec![]), "1");
        assert_res_equal!(oat_file_test("oatprograms/binary_gcd.oat", vec![]), "3");
        assert_res_equal!(oat_file_test("oatprograms/lfsr.oat", vec![]), "TFTF FFTT0");
        assert_res_equal!(
            oat_file_test("oatprograms/gnomesort.oat", vec![]),
            "01253065992000"
        );
        assert_res_equal!(
            oat_file_test("oatprograms/josh_joyce_test.oat", vec![]),
            "0"
        );
        assert_res_equal!(oat_file_test("oatprograms/gcd.oat", vec![]), "16");
        assert_res_equal!(
            oat_file_test("oatprograms/life.oat", vec![]),
            "00101001100101000"
        );
        assert_res_equal!(oat_file_test("oatprograms/lcs.oat", vec![]), "OAT0");
        assert_res_equal!(
            oat_file_test("oatprograms/insertion_sort.oat", vec![]),
            "42"
        );
        assert_res_equal!(
            oat_file_test("oatprograms/maxsubsequence.oat", vec![]),
            "107"
        );
    }
}
