use oatv1::{
    ast::{Exp, Prog, Stmt},
    oat_parser, tokens,
};

#[allow(dead_code)]
pub fn parse_exp(s: &str) -> anyhow::Result<Exp> {
    let lexer = tokens::make_tokenizer(s);
    Ok(oat_parser::exp_topParser::new().parse("no_file", lexer)?)
}
#[allow(dead_code)]
pub fn parse_stmt(s: &str) -> anyhow::Result<Stmt> {
    let lexer = tokens::make_tokenizer(s);
    Ok(oat_parser::stmt_topParser::new().parse("no_file", lexer)?)
}

pub fn parse_file(filepath: &str) -> anyhow::Result<Prog> {
    let source = std::fs::read_to_string(filepath)?;
    let lexer = tokens::make_tokenizer(&source);
    Ok(oat_parser::progParser::new().parse(filepath, lexer)?)
}

#[macro_export]
macro_rules! assert_res_equal {
    ($res:expr, $left:expr) => {
        match $res {
            Ok(res) => k9::assert_equal!(res, $left),
            Err(_) => k9::assert_ok!($res),
        }
    };
}

#[macro_export]
macro_rules! assert_res_equal_parse {
    ($res:expr, $left:expr) => {
        match $res {
            Ok(res) => k9::assert_equal!(res.reset_nodes(), $left),
            Err(_) => k9::assert_ok!($res),
        }
    };
}
