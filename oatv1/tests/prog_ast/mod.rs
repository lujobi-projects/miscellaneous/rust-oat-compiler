use oatv1::{
    assn,
    ast::{
        self, Arg, Binop, Block, CallTy, Decl, Exp, Fdecl, Gdecl, Node, Prog, RetTy, Rty, Stmt, Ty,
        Unop, Vdecl,
    },
    bop, cstr, decl, gfdecl, gvdecl, index, no_loc, no_loc_box, no_loc_vec, node, oat_id,
    oat_id_exp, oat_ref, range, uop,
};
use Binop::*;
use Exp::*;
use Stmt::*;
use Unop::*;

pub fn easy_p1_ast() -> Prog {
    Prog(vec![gfdecl!(
        RetTy::Val(Ty::Int),
        f,
        vec![],
        no_loc_vec![Ret(Some(no_loc!(CInt(0))))]
    )])
}

pub fn easy_p2_ast() -> Prog {
    Prog(vec![gfdecl!(
        RetTy::Val(Ty::Int),
        f,
        vec![Arg(Ty::Int, oat_id!(x))],
        no_loc_vec![
            decl!(x, CInt(0)),
            Assn(
                no_loc!(oat_id_exp!(x)),
                no_loc!(bop!(
                    IOr,
                    bop!(
                        Shl,
                        bop!(
                            Shr,
                            bop!(
                                Sub,
                                (bop!(Add, (oat_id_exp!(x)), (oat_id_exp!(x)))),
                                (bop!(Mul, (oat_id_exp!(x)), (oat_id_exp!(x))))
                            ),
                            oat_id_exp!(x)
                        ),
                        oat_id_exp!(x)
                    ),
                    bop!(
                        IAnd,
                        (oat_id_exp!(x)),
                        bop!(
                            Sar,
                            Uop(Neg, no_loc!(Uop(Bitnot, no_loc!(oat_id_exp!(x))))),
                            oat_id_exp!(x)
                        )
                    )
                ))
            ),
            Ret(Some(no_loc!(oat_id_exp!(x))))
        ]
    )])
}

pub fn easy_p3_ast() -> Prog {
    Prog(vec![
        gfdecl!(
            RetTy::Val(oat_ref!(Rty::String)),
            bar,
            vec![
                Arg(Ty::Int, oat_id!(x)),
                Arg(oat_ref!(Rty::String), oat_id!(y))
            ],
            no_loc_vec![
                decl!(s, CStr("This is a string".to_string())),
                decl!(array, CArr(Ty::Int, no_loc_vec![(CInt(1)), (CInt(3))])),
                decl!(y, Index(no_loc!(oat_id_exp!(array)), no_loc!(CInt(0)))),
                Ret(Some(no_loc!(oat_id_exp!(s))))
            ]
        ),
        gfdecl!(
            RetTy::Void,
            proc1,
            vec![],
            no_loc_vec![
                SCall(CallTy(no_loc!(oat_id_exp!(proc2)), vec![])),
                Ret(None)
            ]
        ),
        gfdecl!(
            RetTy::Void,
            proc2,
            vec![],
            no_loc_vec![
                SCall(CallTy(no_loc!(oat_id_exp!(proc1)), vec![])),
                Ret(None)
            ]
        ),
        gfdecl!(
            RetTy::Val(Ty::Bool),
            foo,
            vec![
                Arg(Ty::Int, oat_id!(x)),
                Arg(oat_ref!(Rty::Array(Ty::Int)), oat_id!(y))
            ],
            no_loc_vec![
                decl!(
                    s,
                    Exp::Call(CallTy(
                        no_loc!(oat_id_exp!(bar)),
                        no_loc_vec![oat_id_exp!(x), cstr!(compilerdesign)]
                    ))
                ),
                SCall(CallTy(no_loc!(oat_id_exp!(proc1)), vec![])),
                Ret(Some(no_loc!(CBool(true))))
            ]
        ),
    ])
}

pub fn easy_p4_ast() -> Prog {
    Prog(vec![
        gfdecl!(
            RetTy::Val(oat_ref!(Rty::String)),
            f,
            vec![],
            no_loc_vec![
                decl!(
                    s,
                    CArr(
                        oat_ref!(Rty::Array(oat_ref!(Rty::String))),
                        no_loc_vec![
                            CArr(
                                oat_ref!(Rty::String),
                                no_loc_vec![
                                    CStr("s00:\n+\n=2*\n".to_string()),
                                    CStr("s01:this is not a comment in string.*".to_string()),
                                    CStr("s02:\"\\t\\n\\\\?\"".to_string())
                                ]
                            ),
                            CArr(
                                oat_ref!(Rty::String),
                                no_loc_vec![
                                    CStr("s10:\u{85}\u{86}".to_string()),
                                    cstr!(s11),
                                    cstr!(s12)
                                ]
                            )
                        ]
                    )
                ),
                Ret(Some(no_loc!(index!(
                    index!(oat_id_exp!(s), CInt(0)),
                    CInt(1)
                ))))
            ]
        ),
        gfdecl!(
            RetTy::Val(oat_ref!(Rty::Array(oat_ref!(Rty::Array(Ty::Int))))),
            g,
            vec![Arg(
                oat_ref!(Rty::Array(oat_ref!(Rty::Array(Ty::Int)))),
                oat_id!(x)
            )],
            no_loc_vec![
                decl!(
                    y,
                    CArr(
                        oat_ref!(Rty::Array(Ty::Int)),
                        no_loc_vec![
                            CArr(Ty::Int, no_loc_vec![CInt(0), CInt(1)]),
                            CArr(Ty::Int, no_loc_vec![CInt(2), CInt(3)])
                        ]
                    )
                ),
                decl!(i, CInt(0)),
                assn!(
                    index!(index!(oat_id_exp!(x), CInt(0)), CInt(0)),
                    bop!(
                        Add,
                        oat_id_exp!(i),
                        index!(index!(oat_id_exp!(y), CInt(1)), CInt(1))
                    )
                ),
                assn!(
                    oat_id_exp!(i),
                    uop!(
                        Neg,
                        uop!(
                            Lognot,
                            uop!(Bitnot, index!(index!(oat_id_exp!(x), CInt(0)), CInt(0)))
                        )
                    )
                ),
                Ret(Some(no_loc!(oat_id_exp!(x))))
            ]
        ),
    ])
}

pub fn easy_p5_ast() -> Prog {
    Prog(vec![
        gvdecl!(i, CInt(19)),
        gvdecl!(b1, CBool(true)),
        gvdecl!(b2, CBool(false)),
        gvdecl!(str, CStr("This is a string!".to_string())),
        gvdecl!(arr1, CArr(Ty::Int, no_loc_vec![CInt(0), CInt(1), CInt(2)])),
        gvdecl!(
            arr2,
            CArr(
                oat_ref!(Rty::Array(Ty::Int)),
                no_loc_vec![
                    CArr(Ty::Int, no_loc_vec![CInt(10), CInt(11)]),
                    CArr(Ty::Int, no_loc_vec![CInt(20), CInt(21)]),
                    CArr(Ty::Int, no_loc_vec![CInt(30), CInt(31)])
                ]
            )
        ),
        gvdecl!(
            arr3,
            CArr(
                oat_ref!(Rty::String),
                no_loc_vec![cstr!(String1), cstr!(String2), cstr!(String3)]
            )
        ),
        gvdecl!(
            arr4,
            CArr(
                oat_ref!(Rty::Array(oat_ref!(Rty::String))),
                no_loc_vec![
                    CArr(
                        oat_ref!(Rty::String),
                        no_loc_vec![cstr!(String00), cstr!(String01)]
                    ),
                    CArr(
                        oat_ref!(Rty::String),
                        no_loc_vec![cstr!(String10), cstr!(String11)]
                    ),
                    CArr(
                        oat_ref!(Rty::String),
                        no_loc_vec![cstr!(String20), cstr!(String21)]
                    )
                ]
            )
        ),
    ])
}

pub fn easy_p6_ast() -> Prog {
    Prog(vec![
        gvdecl!(y, CInt(0)),
        gvdecl!(z, CInt(0)),
        gfdecl!(
            RetTy::Void,
            f,
            vec![Arg(Ty::Int, oat_id!(x)), Arg(Ty::Int, oat_id!(y))],
            no_loc_vec![decl!(x, CInt(0)), Ret(None)]
        ),
        gfdecl!(
            RetTy::Void,
            g,
            vec![Arg(Ty::Int, oat_id!(x)), Arg(Ty::Int, oat_id!(y))],
            no_loc_vec![decl!(z, CInt(0)), Ret(None)]
        ),
    ])
}

pub fn easy_p7_ast() -> Prog {
    Prog(vec![
        Decl::Gvdecl(node!(
            Gdecl {
                name: oat_id!(j),
                init: Box::new(node!(
                    CArr(
                        Ty::Int,
                        vec![
                            node!(CInt(1), range!("oatprograms/easy_p7.oat", 0, 21, 0, 22)),
                            node!(CInt(2), range!("oatprograms/easy_p7.oat", 0, 23, 0, 24)),
                            node!(CInt(3), range!("oatprograms/easy_p7.oat", 0, 25, 0, 26)),
                            node!(CInt(4), range!("oatprograms/easy_p7.oat", 0, 27, 0, 28)),
                        ],
                    ),
                    range!("oatprograms/easy_p7.oat", 0, 11, 0, 29)
                ))
            },
            range!("oatprograms/easy_p7.oat", 0, 0, 0, 30)
        )),
        Decl::Gfdecl(node!(
            Fdecl {
                frtyp: RetTy::Val(oat_ref!(Rty::Array(Ty::Int))),
                fname: oat_id!(f),
                args: vec![],
                body: Block(vec![
                    node!(
                        Decl(Vdecl(
                            oat_id!(a),
                            node!(
                                CArr(
                                    oat_ref!(Rty::Array(Ty::Int)),
                                    vec![
                                        node!(
                                            CInt(1),
                                            range!("oatprograms/easy_p7.oat", 2, 22, 2, 23)
                                        ),
                                        node!(
                                            CInt(2),
                                            range!("oatprograms/easy_p7.oat", 2, 25, 2, 26)
                                        )
                                    ]
                                ),
                                range!("oatprograms/easy_p7.oat", 2, 10, 2, 27)
                            )
                        )),
                        range!("oatprograms/easy_p7.oat", 2, 2, 2, 28)
                    ),
                    node!(
                        Decl(Vdecl(
                            oat_id!(i),
                            node!(
                                NewArr(
                                    Ty::Int,
                                    node!(CInt(4), range!("oatprograms/easy_p7.oat", 3, 18, 3, 19))
                                ),
                                range!("oatprograms/easy_p7.oat", 3, 10, 3, 20)
                            )
                        )),
                        range!("oatprograms/easy_p7.oat", 3, 2, 3, 21)
                    ),
                    node!(
                        Decl(Vdecl(
                            oat_id!(arr1),
                            node!(
                                NewArr(
                                    Ty::Int,
                                    node!(CInt(3), range!("oatprograms/easy_p7.oat", 4, 21, 4, 22))
                                ),
                                range!("oatprograms/easy_p7.oat", 4, 13, 4, 23)
                            )
                        )),
                        range!("oatprograms/easy_p7.oat", 4, 2, 4, 24)
                    ),
                    node!(
                        Ret(Some(node!(
                            NewArr(
                                Ty::Int,
                                node!(CInt(2), range!("oatprograms/easy_p7.oat", 5, 17, 5, 18))
                            ),
                            range!("oatprograms/easy_p7.oat", 5, 9, 5, 19)
                        ))),
                        range!("oatprograms/easy_p7.oat", 5, 2, 5, 20)
                    ),
                ]),
            },
            range!("oatprograms/easy_p7.oat", 1, 0, 6, 1)
        )),
    ])
}
