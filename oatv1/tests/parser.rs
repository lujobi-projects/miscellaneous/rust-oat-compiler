#[macro_use]
mod commons;
mod prog_ast;

mod parser_tests {

    use oatv1::{
        ast::{self, Binop, Block, Exp, NoLocable, Node, Rty, Stmt, Ty, Unop, Vdecl},
        no_loc, oat_id, oat_id_exp,
    };

    use crate::commons::*;
    use crate::prog_ast;

    #[test]
    fn parse_consts() {
        assert_res_equal_parse!(parse_exp("bool[] null"), Exp::CNull(Rty::Array(Ty::Bool)));
        assert_res_equal_parse!(parse_exp("42"), Exp::CInt(42));
        assert_res_equal_parse!(parse_exp("true"), Exp::CBool(true));
        assert_res_equal_parse!(parse_exp("false"), Exp::CBool(false));
        assert_res_equal_parse!(
            parse_exp("\"hello world\""),
            Exp::CStr("hello world".to_string())
        );
        assert_res_equal_parse!(
            parse_exp("new int[]{1, 2, 3}"),
            Exp::CArr(
                Ty::Int,
                vec![
                    no_loc!(Exp::CInt(1)),
                    no_loc!(Exp::CInt(2)),
                    no_loc!(Exp::CInt(3))
                ]
            )
        );
    }

    #[test]
    fn parse_exp_tests() {
        use Binop::*;
        use Exp::*;
        use Ty::*;
        use Unop::*;

        assert_res_equal_parse!(parse_exp("1"), (Exp::CInt(1)));
        assert_res_equal_parse!(
            parse_exp("1+2"),
            (Bop(Add, no_loc!(Exp::CInt(1)), no_loc!(Exp::CInt(2))))
        );
        assert_res_equal_parse!(
            parse_exp("1+2+3"),
            (Bop(
                Add,
                no_loc!(Bop(Add, no_loc!(Exp::CInt(1)), no_loc!(Exp::CInt(2)))),
                no_loc!(Exp::CInt(3))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("1+2*3"),
            (Bop(
                Add,
                no_loc!(Exp::CInt(1)),
                no_loc!(Bop(Mul, no_loc!(Exp::CInt(2)), no_loc!(Exp::CInt(3))))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("1+(2+3)"),
            (Bop(
                Add,
                no_loc!(Exp::CInt(1)),
                no_loc!(Bop(Add, no_loc!(Exp::CInt(2)), no_loc!(Exp::CInt(3))))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("(1+2)*3"),
            (Bop(
                Mul,
                no_loc!(Bop(Add, no_loc!(Exp::CInt(1)), no_loc!(Exp::CInt(2)))),
                no_loc!(Exp::CInt(3))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("1+2*3+4"),
            (Bop(
                Add,
                no_loc!(Bop(
                    Add,
                    no_loc!(Exp::CInt(1)),
                    no_loc!(Bop(Mul, no_loc!(Exp::CInt(2)), no_loc!(Exp::CInt(3))))
                )),
                no_loc!(Exp::CInt(4))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("1-2 == 3+4"),
            (Bop(
                Eq,
                no_loc!(Bop(Sub, no_loc!(CInt(1)), no_loc!(CInt(2)))),
                no_loc!(Bop(Add, no_loc!(CInt(3)), no_loc!(CInt(4))))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("(1+2)*(3+4)"),
            (Bop(
                Mul,
                no_loc!(Bop(Add, no_loc!(CInt(1)), no_loc!(CInt(2)))),
                no_loc!(Bop(Add, no_loc!(CInt(3)), no_loc!(CInt(4))))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("true & true | false"),
            (Bop(
                Or,
                no_loc!(Bop(And, no_loc!(CBool(true)), no_loc!(CBool(true)))),
                no_loc!(CBool(false))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("true & (true | false)"),
            (Bop(
                And,
                no_loc!(CBool(true)),
                no_loc!(Bop(Or, no_loc!(CBool(true)), no_loc!(CBool(false))))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("!(~5 == ~6) & -5+10 < 0"),
            (Bop(
                And,
                no_loc!(Uop(
                    Lognot,
                    no_loc!(Bop(
                        Eq,
                        no_loc!(Uop(Bitnot, no_loc!(CInt(5)))),
                        no_loc!(Uop(Bitnot, no_loc!(CInt(6))))
                    ))
                )),
                no_loc!(Bop(
                    Lt,
                    no_loc!(Bop(
                        Add,
                        no_loc!(Uop(Neg, no_loc!(CInt(5)))),
                        no_loc!(CInt(10))
                    )),
                    no_loc!(CInt(0))
                ))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("1+2 >> (3-4 >>> 7*8) << 9"),
            (Bop(
                Shl,
                no_loc!(Bop(
                    Shr,
                    no_loc!(Bop(Add, no_loc!(CInt(1)), no_loc!(CInt(2)))),
                    no_loc!(Bop(
                        Sar,
                        no_loc!(Bop(Sub, no_loc!(CInt(3)), no_loc!(CInt(4)))),
                        no_loc!(Bop(Mul, no_loc!(CInt(7)), no_loc!(CInt(8))))
                    ))
                )),
                no_loc!(CInt(9))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("~5 >> 7 - 10 < 9 * -6-4 | !false"),
            (Bop(
                Or,
                no_loc!(Bop(
                    Lt,
                    no_loc!(Bop(
                        Shr,
                        no_loc!(Uop(Bitnot, no_loc!(CInt(5)))),
                        no_loc!(Bop(Sub, no_loc!(CInt(7)), no_loc!(CInt(10))))
                    )),
                    no_loc!(Bop(
                        Sub,
                        no_loc!(Bop(
                            Mul,
                            no_loc!(CInt(9)),
                            no_loc!(Uop(Neg, no_loc!(CInt(6))))
                        )),
                        no_loc!(CInt(4))
                    ))
                )),
                no_loc!(Uop(Lognot, no_loc!(CBool(false))))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("false == 2 >= 3 | true !=  9 - 10 <= 4"),
            (Bop(
                Or,
                no_loc!(Bop(
                    Eq,
                    no_loc!(CBool(false)),
                    no_loc!(Bop(Gte, no_loc!(CInt(2)), no_loc!(CInt(3))))
                )),
                no_loc!(Bop(
                    Neq,
                    no_loc!(CBool(true)),
                    no_loc!(Bop(
                        Lte,
                        no_loc!(Bop(Sub, no_loc!(CInt(9)), no_loc!(CInt(10)))),
                        no_loc!(CInt(4))
                    ))
                ))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("1-2*3+4 < 5 | 6+7-2 > 1 | true & false"),
            (Bop(
                Or,
                no_loc!(Bop(
                    Or,
                    no_loc!(Bop(
                        Lt,
                        no_loc!(Bop(
                            Add,
                            no_loc!(Bop(
                                Sub,
                                no_loc!(CInt(1)),
                                no_loc!(Bop(Mul, no_loc!(CInt(2)), no_loc!(CInt(3))))
                            )),
                            no_loc!(CInt(4))
                        )),
                        no_loc!(CInt(5))
                    )),
                    no_loc!(Bop(
                        Gt,
                        no_loc!(Bop(
                            Sub,
                            no_loc!(Bop(Add, no_loc!(CInt(6)), no_loc!(CInt(7)))),
                            no_loc!(CInt(2))
                        )),
                        no_loc!(CInt(1))
                    ))
                )),
                no_loc!(Bop(And, no_loc!(CBool(true)), no_loc!(CBool(false))))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("true [&] false | false [|] true & true"),
            (Bop(
                IOr,
                no_loc!(Bop(
                    IAnd,
                    no_loc!(CBool(true)),
                    no_loc!(Bop(Or, no_loc!(CBool(false)), no_loc!(CBool(false))))
                )),
                no_loc!(Bop(And, no_loc!(CBool(true)), no_loc!(CBool(true))))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("true [|] false [&] true & true | false"),
            (Bop(
                IOr,
                no_loc!(CBool(true)),
                no_loc!(Bop(
                    IAnd,
                    no_loc!(CBool(false)),
                    no_loc!(Bop(
                        Or,
                        no_loc!(Bop(And, no_loc!(CBool(true)), no_loc!(CBool(true)))),
                        no_loc!(CBool(false))
                    ))
                ))
            ))
        );
        assert_res_equal_parse!(parse_exp("new int[3]"), (NewArr(Int, no_loc!(CInt(3)))));
        assert_res_equal_parse!(
            parse_exp("bar (x, \"compilerdesign\")"),
            (Call(ast::CallTy(
                no_loc!(oat_id_exp!(bar)),
                vec![
                    no_loc!(oat_id_exp!(x)),
                    no_loc!(CStr("compilerdesign".to_string()))
                ]
            )))
        );
        assert_res_equal_parse!(parse_exp("new int[3]"), (NewArr(Int, no_loc!(CInt(3)))));
        assert_res_equal_parse!(
            parse_exp("new int[][]{new int[]{10,11},new int[]{20,21},new int[]{30,31}}"),
            (CArr(
                Ref(Box::new(Rty::Array(Int))),
                vec![
                    no_loc!(CArr(Int, vec![no_loc!(CInt(10)), no_loc!(CInt(11))])),
                    no_loc!(CArr(Int, vec![no_loc!(CInt(20)), no_loc!(CInt(21))])),
                    no_loc!(CArr(Int, vec![no_loc!(CInt(30)), no_loc!(CInt(31))]))
                ]
            ))
        );
        assert_res_equal_parse!(
            parse_exp("proc1 ()"),
            (Call(ast::CallTy(no_loc!(oat_id_exp!(proc1)), vec![])))
        );
        assert_res_equal_parse!(
            parse_exp("array[0]"),
            (Exp::Index(no_loc!(oat_id_exp!(array)), no_loc!(CInt(0))))
        );
        assert_res_equal_parse!(
            parse_exp("i + y[1][1]"),
            (Bop(
                Add,
                no_loc!(oat_id_exp!(i)),
                no_loc!(Exp::Index(
                    no_loc!(Exp::Index(no_loc!(oat_id_exp!(y)), no_loc!(CInt(1)))),
                    no_loc!(CInt(1))
                ))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("-!~x[0][0]"),
            (Uop(
                Neg,
                no_loc!(Uop(
                    Lognot,
                    no_loc!(Uop(
                        Bitnot,
                        no_loc!(Exp::Index(
                            no_loc!(Exp::Index(no_loc!(oat_id_exp!(x)), no_loc!(CInt(0)))),
                            no_loc!(CInt(0))
                        ))
                    ))
                ))
            ))
        );
        assert_res_equal_parse!(
            parse_exp("print_string (string_concat (str1, str2))"),
            (Call(ast::CallTy(
                no_loc!(oat_id_exp!(print_string)),
                vec![no_loc!(Call(ast::CallTy(
                    no_loc!(oat_id_exp!(string_concat)),
                    vec![no_loc!(oat_id_exp!(str1)), no_loc!(oat_id_exp!(str2))]
                )))]
            )))
        );
    }

    #[test]
    fn parse_stmt_tests() {
        use Binop::*;
        use Exp::*;
        use Stmt::*;

        assert_res_equal_parse!(
            parse_stmt("var n = 8;"),
            Decl(Vdecl(oat_id!(n), no_loc!(Exp::CInt(8))))
        );
        assert_res_equal_parse!(
            parse_stmt("var x=a[0];"),
            Decl(Vdecl(
                oat_id!(x),
                no_loc!(Exp::Index(no_loc!(oat_id_exp!(a)), no_loc!(Exp::CInt(0))))
            ))
        );
        assert_res_equal_parse!(parse_stmt("return;"), Ret(None));
        assert_res_equal_parse!(
            parse_stmt("return x+y;"),
            Ret(Some(no_loc!(Bop(
                Add,
                no_loc!(oat_id_exp!(x)),
                no_loc!(oat_id_exp!(y))
            ))))
        );
        assert_res_equal_parse!(
            parse_stmt("a[j>>1]=v;"),
            Assn(
                no_loc!(Exp::Index(
                    no_loc!(oat_id_exp!(a)),
                    no_loc!(Bop(Shr, no_loc!(oat_id_exp!(j)), no_loc!(Exp::CInt(1))))
                )),
                no_loc!(oat_id_exp!(v))
            )
        );
        assert_res_equal_parse!(
            parse_stmt("foo(a,1,n);"),
            SCall(ast::CallTy(
                no_loc!(oat_id_exp!(foo)),
                vec![
                    no_loc!(oat_id_exp!(a)),
                    no_loc!(Exp::CInt(1)),
                    no_loc!(oat_id_exp!(n))
                ]
            ))
        );
        assert_res_equal_parse!(
            parse_stmt("a[i]=a[i>>1];"),
            Assn(
                no_loc!(Exp::Index(no_loc!(oat_id_exp!(a)), no_loc!(oat_id_exp!(i)))),
                no_loc!(Exp::Index(
                    no_loc!(oat_id_exp!(a)),
                    no_loc!(Bop(Shr, no_loc!(oat_id_exp!(i)), no_loc!(Exp::CInt(1))))
                ))
            )
        );
        assert_res_equal_parse!(
            parse_stmt("var a = new int[8];"),
            Decl(Vdecl(
                oat_id!(a),
                no_loc!(NewArr(Ty::Int, no_loc!(Exp::CInt(8))))
            ))
        );
        assert_res_equal_parse!(
            parse_stmt("if((j<n)&(a[j]<a[j+1])) { j=j+1; }"),
            If(
                no_loc!(Bop(
                    And,
                    no_loc!(Bop(Lt, no_loc!(oat_id_exp!(j)), no_loc!(oat_id_exp!(n)))),
                    no_loc!(Bop(
                        Lt,
                        no_loc!(Exp::Index(no_loc!(oat_id_exp!(a)), no_loc!(oat_id_exp!(j)))),
                        no_loc!(Exp::Index(
                            no_loc!(oat_id_exp!(a)),
                            no_loc!(Bop(Add, no_loc!(oat_id_exp!(j)), no_loc!(Exp::CInt(1))))
                        ))
                    ))
                )),
                Block(vec![no_loc!(Assn(
                    no_loc!(oat_id_exp!(j)),
                    no_loc!(Bop(Add, no_loc!(oat_id_exp!(j)), no_loc!(Exp::CInt(1))))
                ))]),
                Block(vec![])
            )
        );
        assert_res_equal_parse!(
            parse_stmt("if (c == 1) { var i = 0; var j = 0; var k = 0; }"),
            If(
                no_loc!(Bop(Eq, no_loc!(oat_id_exp!(c)), no_loc!(Exp::CInt(1)))),
                Block(vec![
                    no_loc!(Decl(Vdecl(oat_id!(i), no_loc!(Exp::CInt(0))))),
                    no_loc!(Decl(Vdecl(oat_id!(j), no_loc!(Exp::CInt(0))))),
                    no_loc!(Decl(Vdecl(oat_id!(k), no_loc!(Exp::CInt(0)))))
                ]),
                Block(vec![])
            )
        );
        assert_res_equal_parse!(
            parse_stmt("while((i>1)&(a[i>>1]<v)) { a[i]=a[i>>1]; i=i>>1; }"),
            While(
                no_loc!(Bop(
                    And,
                    no_loc!(Bop(Gt, no_loc!(oat_id_exp!(i)), no_loc!(Exp::CInt(1)))),
                    no_loc!(Bop(
                        Lt,
                        no_loc!(Exp::Index(
                            no_loc!(oat_id_exp!(a)),
                            no_loc!(Bop(Shr, no_loc!(oat_id_exp!(i)), no_loc!(Exp::CInt(1))))
                        )),
                        no_loc!(oat_id_exp!(v))
                    ))
                )),
                Block(vec![
                    no_loc!(Assn(
                        no_loc!(Exp::Index(no_loc!(oat_id_exp!(a)), no_loc!(oat_id_exp!(i)))),
                        no_loc!(Exp::Index(
                            no_loc!(oat_id_exp!(a)),
                            no_loc!(Bop(Shr, no_loc!(oat_id_exp!(i)), no_loc!(Exp::CInt(1))))
                        ))
                    )),
                    no_loc!(Assn(
                        no_loc!(oat_id_exp!(i)),
                        no_loc!(Bop(Shr, no_loc!(oat_id_exp!(i)), no_loc!(Exp::CInt(1))))
                    ))
                ])
            )
        );

        let x = "for (; i > 0; i=i-1;) { 
                           for (var j = 1; j <= i; j=j+1;) { 
                               if (numbers[j-1] > numbers[i]) { 
                                   temp = numbers[j-1]; 
                                   numbers[j-1] = numbers[i]; 
                                   numbers[i] = temp; 
                               } 
                           } 
                       }";

        assert_res_equal_parse!(
            parse_stmt(x),
            For(
                vec![],
                Some(no_loc!(Bop(
                    Gt,
                    no_loc!(oat_id_exp!(i)),
                    no_loc!(Exp::CInt(0))
                ))),
                Some(no_loc!(Assn(
                    no_loc!(oat_id_exp!(i)),
                    no_loc!(Bop(Sub, no_loc!(oat_id_exp!(i)), no_loc!(Exp::CInt(1))))
                ))),
                Block(vec![no_loc!(For(
                    vec![Vdecl(oat_id!(j), no_loc!(Exp::CInt(1)))],
                    Some(no_loc!(Bop(
                        Lte,
                        no_loc!(oat_id_exp!(j)),
                        no_loc!(oat_id_exp!(i))
                    ))),
                    Some(no_loc!(Assn(
                        no_loc!(oat_id_exp!(j)),
                        no_loc!(Bop(Add, no_loc!(oat_id_exp!(j)), no_loc!(Exp::CInt(1))))
                    ))),
                    Block(vec![no_loc!(If(
                        no_loc!(Bop(
                            Gt,
                            no_loc!(Exp::Index(
                                no_loc!(oat_id_exp!(numbers)),
                                no_loc!(Bop(Sub, no_loc!(oat_id_exp!(j)), no_loc!(Exp::CInt(1))))
                            )),
                            no_loc!(Exp::Index(
                                no_loc!(oat_id_exp!(numbers)),
                                no_loc!(oat_id_exp!(i))
                            ))
                        )),
                        Block(vec![
                            no_loc!(Assn(
                                no_loc!(oat_id_exp!(temp)),
                                no_loc!(Exp::Index(
                                    no_loc!(oat_id_exp!(numbers)),
                                    no_loc!(Bop(
                                        Sub,
                                        no_loc!(oat_id_exp!(j)),
                                        no_loc!(Exp::CInt(1))
                                    ))
                                ))
                            )),
                            no_loc!(Assn(
                                no_loc!(Exp::Index(
                                    no_loc!(oat_id_exp!(numbers)),
                                    no_loc!(Bop(
                                        Sub,
                                        no_loc!(oat_id_exp!(j)),
                                        no_loc!(Exp::CInt(1))
                                    ))
                                )),
                                no_loc!(Exp::Index(
                                    no_loc!(oat_id_exp!(numbers)),
                                    no_loc!(oat_id_exp!(i))
                                ))
                            )),
                            no_loc!(Assn(
                                no_loc!(Exp::Index(
                                    no_loc!(oat_id_exp!(numbers)),
                                    no_loc!(oat_id_exp!(i))
                                )),
                                no_loc!(oat_id_exp!(temp))
                            )),
                        ]),
                        Block(vec![])
                    ))])
                ))])
            )
        );

        assert_res_equal_parse!(
            parse_stmt("for (var i = 0, var j = 0; ;) { }"),
            For(
                vec![
                    Vdecl(oat_id!(i), no_loc!(Exp::CInt(0))),
                    Vdecl(oat_id!(j), no_loc!(Exp::CInt(0)))
                ],
                None,
                None,
                Block(vec![])
            )
        );
    }

    #[test]
    fn parse_prog_tests() {
        assert_res_equal_parse!(
            parse_file("oatprograms/easy_p1.oat"),
            prog_ast::easy_p1_ast()
        );
        assert_res_equal_parse!(
            parse_file("oatprograms/easy_p2.oat"),
            prog_ast::easy_p2_ast()
        );
        assert_res_equal_parse!(
            parse_file("oatprograms/easy_p3.oat"),
            prog_ast::easy_p3_ast()
        );
        assert_res_equal_parse!(
            parse_file("oatprograms/easy_p4.oat"),
            prog_ast::easy_p4_ast()
        );
        assert_res_equal_parse!(
            parse_file("oatprograms/easy_p5.oat"),
            prog_ast::easy_p5_ast()
        );
        assert_res_equal_parse!(
            parse_file("oatprograms/easy_p6.oat"),
            prog_ast::easy_p6_ast()
        );
        k9::assert_ok!(parse_file("oatprograms/easy_p7.oat"));
        k9::assert_equal!(
            parse_file("oatprograms/easy_p7.oat").unwrap(),
            prog_ast::easy_p7_ast()
        );
        // // println!("{:?}", x.reset_nodes());
        // // println!("{:?}", y);
        // // k9::assert_equal!(x,)
    }
}
