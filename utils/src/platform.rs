use std::path::Path;
use std::process::Command;

use uuid::Uuid;

// const dot_path: &str = "./";

const LIBS: &[&str] = &[];
const LIB_PATHS: &[&str] = &[];
const LIB_SEARCH_PATHS: &[&str] = &[];
const INCLUDE_PATHS: &[&str] = &[];

const COMMON_FLAGS: &[&str] = &["-Wno-override-module"];
// const clang_ll_mode: &[&str] = &["-S"];
// const as_mode: &[&str] = &["-c"];
const OPT_LEVEL: &[&str] = &["-O1", "-Wall"];
const PLATFORM_FLAGS: &[&str] = &[];

pub fn sh<T>(
    cmd: &mut std::process::Command,
    ret: Box<dyn FnOnce(&str, i32) -> anyhow::Result<T>>,
) -> anyhow::Result<T> {
    let output = cmd.output()?;
    ret(format!("{:?}", cmd).as_str(), output.status.code().unwrap())
}

pub fn gen_name(basedir: &str, basen: &str, baseext: &str) -> anyhow::Result<String> {
    let file_name = || format!("{}/{}_{}{}", basedir, basen, Uuid::new_v4(), baseext);
    loop {
        let f_n = file_name();
        let file = Path::new(&f_n);
        if let Some(p) = file.parent() {
            if !p.exists() {
                std::fs::create_dir_all(p)?;
            }
            if !file.exists() {
                return Ok(f_n);
            }
        }
    }
}

pub fn clang(args: Vec<&str>) -> String {
    format!("clang {} -o ", args.join(" "))
}

pub fn link_args() -> Vec<String> {
    vec![
        COMMON_FLAGS.to_vec(),
        OPT_LEVEL.to_vec(),
        PLATFORM_FLAGS.to_vec(),
    ]
    .into_iter()
    .flatten()
    .map(|s| s.to_string())
    .collect()
}

fn raise_error(cmd: &str, status: i32) -> anyhow::Result<()> {
    if status != 0 {
        anyhow::bail!("{} failed with status {}", cmd, status)
    }
    Ok(())
}

pub fn ignore_error(_cmd: &str, _status: i32) -> anyhow::Result<()> {
    Ok(())
}

pub fn link(mods: Vec<&String>, out_fn: &String) -> anyhow::Result<()> {
    let mut cmd = Command::new("clang");
    cmd.args(link_args())
        .arg("-o")
        .arg(out_fn)
        .args(mods)
        .args(LIB_PATHS.to_vec())
        .args(LIB_SEARCH_PATHS.iter().map(|p| format!("-L{}", p)))
        .args(INCLUDE_PATHS.iter().map(|p| format!("-I{}", p)))
        .args(LIBS.iter().map(|p| format!("-l{}", p)));
    sh(&mut cmd, Box::new(raise_error))
}
