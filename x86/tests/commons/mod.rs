#[macro_export]
macro_rules! assert_res_equal {
    ($res:expr, $left:expr) => {
        match $res {
            Ok(res) => k9::assert_equal!(res, $left),
            Err(_) => k9::assert_ok!($res),
        }
    };
}
