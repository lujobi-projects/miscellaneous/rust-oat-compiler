use std::{
    collections::HashMap,
    ops::{Shl, Shr},
};

// X86lite Simulator
use thiserror::Error;

use crate::*;

/* See the documentation in the X86lite specification, available on the
course web pages, for a detailed explanation of the instruction
semantics. */

// simulator machine state --------------------------------------------------
pub const MEM_BOT: i64 = 0x400000; // lowest valid address
pub const MEM_TOP: i64 = 0x410000; // one past the last byte in memory
                                   // const MEM_TOP: i64 = 0x400100; // one past the last byte in memory
pub const MEM_SIZE: i64 = MEM_TOP - MEM_BOT;
pub const NREGS: i64 = 17; // including Rip
pub const INST_SIZE: usize = 8; // assume we have a 8-byte encoding
pub const EXIT_ADDR: i64 = 0xfdead; // halt when m.regs(%rip) = exit_addr

/* Your simulator should raise this exception if it tries to read from or
store to an address not within the valid address space. */
#[derive(Debug, Clone, Error)]
pub enum X86liteSegfault {
    #[error("undefined memory {0}")]
    UndefinedSym(Lbl),
    #[error("Redefinition of symbol {0}")]
    RedefinedSym(Lbl),
}
pub type X86LiteResult<T> = Result<T, X86liteSegfault>;
// impl fmt::Display for X86liteSegfault {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         write!(f, "X86lite_segfault")
//     }
// }

/*
  The simulator memory maps addresses to symbolic bytes.  Symbolic
  bytes are either actual data indicated by the Byte constructor or
  'symbolic instructions' that take up eight bytes for the purposes of
  layout.

  The symbolic bytes abstract away from the details of how
  instructions are represented in memory.  Each instruction takes
  exactly eight consecutive bytes, where the first byte InsB0 stores
  the actual instruction, and the next sevent bytes are InsFrag
  elements, which aren't valid data.

  For example, the two-instruction sequence:
       at&t syntax             ocaml syntax
     movq %rdi, (%rsp)       Movq,  [~%Rdi; Ind2 Rsp]
     decq %rdi               Decq,  [~%Rdi]

  is represented by the following elements of the mem array (starting
  at address 0x400000):

      0x400000 :  InsB0 (Movq,  [~%Rdi; Ind2 Rsp])
      0x400001 :  InsFrag
      0x400002 :  InsFrag
      0x400003 :  InsFrag
      0x400004 :  InsFrag
      0x400005 :  InsFrag
      0x400006 :  InsFrag
      0x400007 :  InsFrag
      0x400008 :  InsB0 (Decq,  [~%Rdi])
      0x40000A :  InsFrag
      0x40000B :  InsFrag
      0x40000C :  InsFrag
      0x40000D :  InsFrag
      0x40000E :  InsFrag
      0x40000F :  InsFrag
      0x400010 :  InsFrag
*/

#[derive(Clone, Debug, PartialEq)]
pub enum SByte {
    InsB0(Ins), // 1st byte of an instruction
    InsFrag,    // 2nd - 8th bytes of an instruction
    Byte(u8),   // non-instruction byte
}
#[macro_export]
macro_rules! cbyte {
    ($x:expr) => {
        SByte::Byte($x as u8)
    };
}

#[macro_export]
macro_rules! sbyte_insns {
    ($($args:expr),*) => {
        vec![
            $(SByte::InsB0($args), // 1st byte of an instruction
            SByte::InsFrag,
            SByte::InsFrag,
            SByte::InsFrag,
            SByte::InsFrag,
            SByte::InsFrag,
            SByte::InsFrag,
            SByte::InsFrag,)* // 2nd - 8th bytes of an instruction
        ]
    };
}
// memory maps addresses to symbolic bytes
type Mem = Vec<SByte>;

// Flags for condition codes
#[derive(Clone, Debug, PartialEq)]
pub struct Flags {
    pub fo: bool,
    pub fs: bool,
    pub fz: bool,
}

// Register files
type Regs = Vec<i64>;

// Complete machine state
#[derive(Clone, Debug)]
pub struct Mach {
    pub flags: Flags,
    pub regs: Regs,
    pub mem: Mem,
}

// simulator helper functions -----------------------------------------------

// The index of a register in the regs array
pub fn rind(reg: &Reg) -> usize {
    match reg {
        Reg::Rip => 16,
        Reg::Rax => 0,
        Reg::Rbx => 1,
        Reg::Rcx => 2,
        Reg::Rdx => 3,
        Reg::Rsi => 4,
        Reg::Rdi => 5,
        Reg::Rbp => 6,
        Reg::Rsp => 7,
        Reg::R08 => 8,
        Reg::R09 => 9,
        Reg::R10 => 10,
        Reg::R11 => 11,
        Reg::R12 => 12,
        Reg::R13 => 13,
        Reg::R14 => 14,
        Reg::R15 => 15,
    }
}

// Helper functions for reading/writing sbytes

// Convert an int64 to its sbyte representation
pub fn sbytes_of_i64(i: i64) -> Vec<SByte> {
    vec![0, 8, 16, 24, 32, 40, 48, 56]
        .into_iter()
        .map(|n: u8| SByte::Byte((i >> n & 0xff) as u8))
        .collect()
}

// Convert an sbyte representation to an int64
pub fn int64_of_sbytes(bs: Vec<SByte>) -> i64 {
    bs.into_iter().rev().fold(0, |acc, item| match item {
        SByte::Byte(c) => (acc << INST_SIZE) | (c as i64),
        _ => 0,
    })
}

// Convert a string to its sbyte representation
fn sbytes_of_string(s: String) -> Vec<SByte> {
    let mut res = vec![SByte::Byte(0x00)];
    let string_iter = s.chars().into_iter().rev();
    res.reserve(s.len());
    string_iter.for_each(|item| res.push(SByte::Byte(item as u8)));
    res.reverse();
    res
}

// Serialize an instruction to sbytes
fn sbytes_of_ins(ins: Ins) -> Vec<SByte> {
    let check = |x: &Operand| match x {
        Operand::Imm(Imm::Lbl(_)) | Operand::Ind1(Imm::Lbl(_)) | Operand::Ind3(Imm::Lbl(_), _) => {
            panic!("sbytes_of_ins: tried to serialize a label!")
        }
        _ => (),
    };
    ins.1.iter().for_each(check);
    vec![
        SByte::InsB0(ins),
        SByte::InsFrag,
        SByte::InsFrag,
        SByte::InsFrag,
        SByte::InsFrag,
        SByte::InsFrag,
        SByte::InsFrag,
        SByte::InsFrag,
    ]
}

// Serialize a data element to sbytes
fn sbytes_of_data(data: &Data) -> Vec<SByte> {
    match data {
        Data::Quad(Imm::Lit(i)) => sbytes_of_i64(i.to_owned()),
        Data::Asciz(s) => sbytes_of_string(s.clone()),
        Data::Quad(Imm::Lbl(_)) => panic!("sbytes_of_data: tried to serialize a label!"),
    }
}

/* It might be useful to toggle printing of intermediate states of your
  simulator. Our implementation uses this mutable flag to turn on/off
  printing.  For instance, you might write something like:

    [if !debug_simulator then print_endline @@ string_of_ins u; ...]
*/
const DEBUG_SIMULATOR: bool = false;

fn debug_sim(s: &str) {
    if DEBUG_SIMULATOR {
        println!("{}", s);
    }
}

// Interpret a condition code with respect to the given flags.
pub fn interp_cnd(cnd: Cnd, flags: &Flags) -> bool {
    match cnd {
        Cnd::Eq => flags.fz,
        Cnd::Neq => !flags.fz,
        Cnd::Lt => flags.fs != flags.fo,
        Cnd::Le => flags.fs != flags.fo || flags.fz,
        Cnd::Gt => flags.fs == flags.fo && !flags.fz,
        Cnd::Ge => flags.fs == flags.fo,
    }
}

/* Maps an X86lite address into Some OCaml array index,
or None if the address is not within the legal address space. */
pub fn map_addr(addr: Quad) -> Option<usize> {
    if !(MEM_BOT..MEM_TOP).contains(&addr) {
        debug_sim("X86_segmentation_fault, address not in legal adress space");
        return None;
    }
    Some((addr - MEM_BOT).try_into().unwrap())
}

fn filter_immediate(i: &Imm) -> i64 {
    if let Imm::Lit(x) = i {
        return x.to_owned();
    }
    debug_sim("Label appeared in machine code");
    panic!("X86lite_segfault")
}

fn get_op_val(m: &Mach, o: &Operand) -> i64 {
    let mem2byte = |start| {
        let mut result = vec![];
        result.reserve(8);
        for i in start..start + 8 {
            result.push(m.mem[i as usize].clone());
        }
        int64_of_sbytes(result)
    };
    match o {
        Operand::Imm(x) => filter_immediate(x),
        Operand::Reg(r) => m.regs[rind(r)],
        Operand::Ind1(i) => {
            mem2byte(map_addr(filter_immediate(i)).expect("Ind1, X86lite_segfault"))
        }
        Operand::Ind2(r) => mem2byte(map_addr(m.regs[rind(r)]).expect("Ind2, X86lite_segfault")),
        Operand::Ind3(displ, r) => {
            let addr = m.regs[rind(r)] + filter_immediate(displ);
            mem2byte(map_addr(addr).expect("Ind1, X86lite_segfault"))
        }
    }
}

fn calculate_ind_addr(m: &Mach, o: &Operand) -> i64 {
    match o {
        Operand::Ind1(i) => filter_immediate(i),
        Operand::Ind2(r) => m.regs[rind(r)],
        Operand::Ind3(displ, r) => m.regs[rind(r)] + filter_immediate(displ),
        _ => {
            debug_sim("Not an indirect operand");
            panic!("X86lite_segfault")
        }
    }
}

fn set_op_value(m: &mut Mach, o: &Operand, v: i64) {
    match o {
        Operand::Reg(r) => m.regs[rind(r)] = v,
        Operand::Ind1(_) | Operand::Ind2(_) | Operand::Ind3(_, _) => {
            let mem_ind = map_addr(calculate_ind_addr(m, o)).expect("error decoding");
            let byte_array = sbytes_of_i64(v);
            m.mem.splice(mem_ind..(mem_ind + 8), byte_array);
        }
        _ => {
            debug_sim("Not an indirect operand");
            panic!("X86lite_segfault")
        }
    }
}

fn process_arithmetic(m: &mut Mach, i: Ins) {
    fn set_flags_bitmanip(res: i64, shift_amount: i64, flags: &mut Flags) -> i64 {
        if shift_amount == 0 {
            return res;
        }
        flags.fz = res == 0;
        flags.fs = res < 0;
        flags.fo = flags.fo && (shift_amount != 1);
        res
    }

    fn set_flags(res: (i64, bool), flags: &mut Flags) -> i64 {
        flags.fz = res.0 == 0;
        flags.fs = res.0 < 0;
        flags.fo = res.1;
        res.0
    }

    fn opcode_to_fun(op: Opcode) -> Box<dyn FnMut(i64, i64, &mut Mach) -> i64> {
        use crate::Opcode::*;
        match op {
            Addq => Box::new(|x: i64, y, m| set_flags(x.overflowing_add(y), &mut m.flags)),
            Cmpq | Subq => Box::new(|x: i64, y, m| set_flags(y.overflowing_sub(x), &mut m.flags)),
            Imulq => Box::new(|x: i64, y, m| set_flags(x.overflowing_mul(y), &mut m.flags)),
            Xorq => Box::new(|x: i64, y, m| set_flags((x ^ (y), false), &mut m.flags)),
            Orq => Box::new(|x: i64, y, m| set_flags((x | (y), false), &mut m.flags)),
            Andq => Box::new(|x: i64, y, m| set_flags((x & (y), false), &mut m.flags)),
            Incq => Box::new(|x: i64, _, m| set_flags(x.overflowing_add(1), &mut m.flags)),
            Decq => Box::new(|x: i64, _, m| set_flags(x.overflowing_sub(1), &mut m.flags)),
            Negq => Box::new(|x: i64, _, m| set_flags(0i64.overflowing_sub(x), &mut m.flags)),
            Sarq => Box::new(|x: i64, y, m| {
                let y = y;
                set_flags_bitmanip(x.shr(y), y, &mut m.flags)
            }),
            Shlq => Box::new(|x: i64, y, m| {
                let y = y;
                set_flags_bitmanip((x as u64).shl(y).try_into().unwrap(), y, &mut m.flags)
            }),
            Shrq => Box::new(|x: i64, y, m| {
                let y = y as u64;
                set_flags_bitmanip(
                    (x as u64).shr(y).try_into().unwrap(),
                    y as i64,
                    &mut m.flags,
                )
            }),
            Notq => Box::new(|x: i64, _, m| set_flags(x.overflowing_sub(1), &mut m.flags)),
            _ => panic!("X86segfault"),
        }
    }

    match (i.0, i.1.as_slice()) {
        (Opcode::Cmpq, [x, y, ..]) => {
            opcode_to_fun(Opcode::Cmpq)(get_op_val(m, x), get_op_val(m, y), m);
        }
        (op, [x, y, ..]) => {
            let res = opcode_to_fun(op)(get_op_val(m, x), get_op_val(m, y), m);
            set_op_value(m, y, res)
        }
        (op, [x, ..]) => {
            let res = opcode_to_fun(op)(get_op_val(m, x), 0, m);
            set_op_value(m, x, res)
        }
        _ => {
            panic!("Too few operands \n");
        }
    }
}

fn process_mem(m: &mut Mach, i: Ins) {
    match (i.0, i.1.as_slice()) {
        (Opcode::Movq, [x, y, ..]) => set_op_value(m, y, get_op_val(m, x)),
        (Opcode::Pushq, [x, ..]) => {
            m.regs[rind(&Reg::Rsp)] -= INST_SIZE as i64;
            set_op_value(m, &Operand::Ind2(Reg::Rsp), get_op_val(m, x));
        }
        (Opcode::Popq, [x, ..]) => {
            set_op_value(m, x, get_op_val(m, &Operand::Ind2(Reg::Rsp)));
            m.regs[rind(&Reg::Rsp)] += INST_SIZE as i64;
        }
        _ => {
            debug_sim("Not a data movement Operand, stupid!");
            panic!("X86lite_segfault")
        }
    }
}

fn process_set(m: &mut Mach, cc: Cnd, dst: &Operand) {
    if !interp_cnd(cc, &m.flags) {
        return;
    }
    match dst {
        Operand::Reg(r) => m.regs[rind(r)] = 1,
        Operand::Ind1(_) | Operand::Ind2(_) | Operand::Ind3(_, _) => {
            let mem_ind = map_addr(calculate_ind_addr(m, dst)).expect("ueli");
            m.mem[mem_ind] = sbytes_of_i64(1)[0].clone()
        }
        _ => {
            debug_sim("Not a data movement Operand, stupid!");
            panic!("X86lite_segfault")
        }
    }
}

fn incr_rip(m: &mut Mach) {
    m.regs[rind(&Reg::Rip)] += INST_SIZE as i64
}

/* Simulates one step of the machine:
   - fetch the instruction at %rip
   - compute the source and/or destination information from the operands
   - simulate the instruction semantics
   - update the registers and/or memory appropriately
   - set the condition flags
*/
pub fn step(m: &mut Mach) {
    let ind = m.regs[rind(&Reg::Rip)];
    let addr = map_addr(ind).expect("wellwellwelll");
    if let SByte::InsB0(i) = m.mem[addr].clone() {
        debug_sim(format!("instruction {i}").as_str());
        // println!("instruction {}", string_of_ins(&i).as_str());

        use crate::Opcode::*;
        use crate::Operand::*;
        use crate::Reg::*;
        match (i.0, i.1.as_slice()) {
            (Movq, _) | (Pushq, _) | (Popq, _) => {
                process_mem(m, i);
                incr_rip(m);
            }
            (Addq, _)
            | (Subq, _)
            | (Imulq, _)
            | (Xorq, _)
            | (Orq, _)
            | (Andq, _)
            | (Incq, _)
            | (Decq, _)
            | (Notq, _)
            | (Negq, _)
            | (Sarq, _)
            | (Shlq, _)
            | (Shrq, _)
            | (Cmpq, _) => {
                process_arithmetic(m, i);
                incr_rip(m);
            }
            (Retq, _) => process_mem(m, Ins(Popq, vec![Reg(Rip)])),
            (Jmp, [x]) | (Jmp, [x, _]) => m.regs[rind(&Rip)] = get_op_val(m, x),
            (Callq, [x]) | (Callq, [x, _]) => {
                incr_rip(m);
                process_mem(m, Ins(Pushq, vec![Reg(Rip)]));
                m.regs[rind(&Rip)] = get_op_val(m, x)
            }
            (J(cc), [x]) | (J(cc), [x, _]) => {
                if interp_cnd(cc, &m.flags) {
                    m.regs[rind(&Rip)] = get_op_val(m, x)
                } else {
                    incr_rip(m)
                }
            }
            (Set(cc), [x]) | (Set(cc), [x, _]) => {
                process_set(m, cc, x);
                incr_rip(m)
            }
            (Leaq, [op, dst]) | (Leaq, [op, dst, _]) => {
                set_op_value(m, dst, calculate_ind_addr(&m.clone(), op))
            }
            _ => {
                debug_sim("Malformatted Instruction!");
                dbg!((i.0, i.1.as_slice()));
                panic!("X86lite_segfault")
            }
        }
    } else {
        debug_sim("WTF");
        panic!("X86lite_segfault")
    }
}

/* Runs the machine until the rip register reaches a designated
memory address. Returns the contents of %rax when the
machine halts. */
pub fn run(mut m: Mach) -> i64 {
    let instr_ptr = rind(&Reg::Rip);
    while m.regs[instr_ptr] != EXIT_ADDR {
        step(&mut m)
    }
    m.regs[rind(&Reg::Rax) as usize] as i64
}

// assembling and linking ---------------------------------------------------

// A representation of the executable
#[derive(Clone, Debug)]
pub struct Exec {
    pub entry: Quad,          // address of the entry point
    pub text_pos: Quad,       // starting address of the code
    pub data_pos: Quad,       // starting address of the data
    pub text_seg: Vec<SByte>, // contents of the text segment
    pub data_seg: Vec<SByte>, // contents of the data segment
}

// Assemble should raise this when a label is used but not defined
pub fn undefined_sym(lbl: Lbl) {
    panic!("Undefined Symbol: {}", lbl)
}

// Assemble should raise this when a label is defined more than once
pub fn redefined_sym(lbl: Lbl) {
    panic!("Redefined Symbol: {}", lbl)
}

/* Convert an X86 program into an object file:
 - separate the text and data segments
 - compute the size of each segment
    Note: the size of an Asciz string section is (1 + the string length)
          due to the null terminator

 - resolve the labels to concrete addresses and 'patch' the instructions to
   replace Lbl values with the corresponding Imm values.

 - the text segment starts at the lowest address
 - the data segment starts after the text segment

HINT: List.fold_left and List.fold_right are your friends.
 */

type LabelMap = HashMap<Lbl, i64>;
struct AddrElem {
    acc: i64,
    lbls: LabelMap,
}

struct DataElem {
    lbl: Lbl,
    repr: Vec<SByte>,
}

struct TextElem {
    lbl: Lbl,
    // global: bool,
    asm: Vec<Ins>,
}

fn process_text(old: AddrElem, el: &TextElem) -> AddrElem {
    let mut new_lbls = old.lbls;
    new_lbls.insert(el.lbl.clone(), old.acc);
    let new_acc = old.acc + (INST_SIZE * el.asm.len()) as i64;
    AddrElem {
        acc: new_acc,
        lbls: new_lbls,
    }
}

fn find_label_address(lbl_map: LabelMap, s: Lbl) -> X86LiteResult<i64> {
    if let Some(ind) = lbl_map.get(&s) {
        return Ok(ind.to_owned());
    }
    Err(X86liteSegfault::UndefinedSym(s))
}
fn process_text_2(lbl_map: LabelMap, ins: Ins) -> X86LiteResult<Vec<SByte>> {
    let replace_label = |arg: &Operand| -> X86LiteResult<Operand> {
        match arg {
            Operand::Imm(Imm::Lbl(s)) => Ok(Operand::Imm(Imm::Lit(find_label_address(
                lbl_map.clone(),
                s.clone(),
            )?))),
            Operand::Ind1(Imm::Lbl(s)) => Ok(Operand::Ind1(Imm::Lit(find_label_address(
                lbl_map.clone(),
                s.clone(),
            )?))),
            Operand::Ind3(Imm::Lbl(s), x) => Ok(Operand::Ind3(
                Imm::Lit(find_label_address(lbl_map.clone(), s.clone())?),
                x.clone(),
            )),
            x => Ok(x.clone()),
        }
    };
    let new_ins = Ins(
        ins.0,
        ins.1.iter().map(replace_label).collect::<Result<_, _>>()?,
    );
    Ok(sbytes_of_ins(new_ins))
}

fn process_data(old: AddrElem, el: &DataElem) -> AddrElem {
    let mut new_lbls = old.lbls;
    new_lbls.insert(el.lbl.clone(), old.acc);
    let new_acc = old.acc + (el.repr.len() as i64);
    AddrElem {
        acc: new_acc,
        lbls: new_lbls,
    }
}

pub fn assemble(p: Prog) -> Result<Exec, X86liteSegfault> {
    let filter_data = |el: &Elem| match el.asm.to_owned() {
        Asm::Data(x) => Some(DataElem {
            lbl: el.lbl.clone(),
            repr: x.iter().flat_map(sbytes_of_data).collect(),
        }),
        _ => None,
    };

    let filter_text = |el: &Elem| match el.asm.to_owned() {
        Asm::Text(x) => Some(TextElem {
            lbl: el.lbl.clone(),
            // global: el.global,
            asm: x,
        }),
        _ => None,
    };

    let data: Vec<DataElem> = p.0.iter().filter_map(filter_data).collect();
    let text: Vec<TextElem> = p.0.iter().filter_map(filter_text).collect();

    let text_lbl_map = text.iter().fold(
        AddrElem {
            acc: MEM_BOT,
            lbls: LabelMap::new(),
        },
        process_text,
    );

    let data_lbl_map = data.iter().fold(
        AddrElem {
            acc: text_lbl_map.acc,
            lbls: LabelMap::new(),
        },
        process_data,
    );

    let lbl_map: LabelMap = data_lbl_map
        .lbls
        .into_iter()
        .chain(text_lbl_map.lbls)
        .collect();

    let inst_list: Vec<Ins> = text.iter().flat_map(|x| x.asm.clone()).collect();
    let data_seg: Vec<SByte> = data.iter().flat_map(|x| x.repr.clone()).collect();
    let text_seg = inst_list
        .iter()
        .map(|x| process_text_2(lbl_map.clone(), x.clone()))
        .collect::<Result<Vec<Vec<SByte>>, _>>()?
        .iter()
        .flatten()
        .cloned()
        .collect::<Vec<_>>();

    Ok(Exec {
        entry: find_label_address(lbl_map, String::from("main"))?,
        text_pos: MEM_BOT,
        data_pos: MEM_BOT + (text_seg.len() as i64),
        text_seg,
        data_seg,
    })
}

/* Convert an object file into an executable machine state.
  - allocate the mem array
  - set up the memory state by writing the symbolic bytes to the
    appropriate locations
  - create the inital register state
    - initialize rip to the entry point address
    - initializes rsp to the last word in memory
    - the other registers are initialized to 0
  - the condition code flags start as 'false'

Hint: The Array.make, Array.blit, and Array.of_list library functions
may be of use.
 */

pub fn load(exec: Exec) -> Mach {
    let mut regs = vec![0; NREGS as usize];
    regs[rind(&Reg::Rip)] = exec.entry;
    regs[rind(&Reg::Rsp)] = MEM_TOP - INST_SIZE as i64;

    let mut mem = vec![SByte::InsFrag; MEM_SIZE as usize];

    let text_start = (exec.text_pos - MEM_BOT) as usize;
    let text_stop = text_start + exec.text_seg.len();
    mem.splice(text_start..text_stop, exec.text_seg.iter().cloned());

    let data_start = (exec.data_pos - MEM_BOT) as usize;
    let data_stop = data_start + exec.data_seg.len();
    mem.splice(data_start..data_stop, exec.data_seg.iter().cloned());

    let ins_start = ((MEM_TOP - MEM_BOT) as usize) - INST_SIZE;
    let ins_stop = ins_start + INST_SIZE;
    mem.splice(ins_start..ins_stop, sbytes_of_i64(EXIT_ADDR));

    Mach {
        flags: Flags {
            fo: false,
            fs: false,
            fz: false,
        },
        regs,
        mem,
    }
}
