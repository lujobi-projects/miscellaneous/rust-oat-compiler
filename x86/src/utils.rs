use crate::*;
use core::fmt;

impl std::fmt::Display for Reg {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Reg::Rip => write!(f, "%rip"),
            Reg::Rax => write!(f, "%rax"),
            Reg::Rbx => write!(f, "%rbx"),
            Reg::Rcx => write!(f, "%rcx"),
            Reg::Rdx => write!(f, "%rdx"),
            Reg::Rsi => write!(f, "%rsi"),
            Reg::Rdi => write!(f, "%rdi"),
            Reg::Rbp => write!(f, "%rbp"),
            Reg::Rsp => write!(f, "%rsp"),
            Reg::R08 => write!(f, "%r8 "),
            Reg::R09 => write!(f, "%r9 "),
            Reg::R10 => write!(f, "%r10"),
            Reg::R11 => write!(f, "%r11"),
            Reg::R12 => write!(f, "%r12"),
            Reg::R13 => write!(f, "%r13"),
            Reg::R14 => write!(f, "%r14"),
            Reg::R15 => write!(f, "%r15"),
        }
    }
}

// used if only accessing one byte
pub fn string_of_byte_reg(reg: &Reg) -> String {
    match reg {
        Reg::Rip => panic!("%rip used as byte register"),
        Reg::Rax => String::from("%al"),
        Reg::Rbx => String::from("%bl"),
        Reg::Rcx => String::from("%cl"),
        Reg::Rdx => String::from("%dl"),
        Reg::Rsi => String::from("%sil"),
        Reg::Rdi => String::from("%dil"),
        Reg::Rbp => String::from("%bpl"),
        Reg::Rsp => String::from("%spl"),
        Reg::R08 => String::from("%r8b"),
        Reg::R09 => String::from("%r9b"),
        Reg::R10 => String::from("%r10b"),
        Reg::R11 => String::from("%r11b"),
        Reg::R12 => String::from("%r12b"),
        Reg::R13 => String::from("%r13b"),
        Reg::R14 => String::from("%r14b"),
        Reg::R15 => String::from("%r15b"),
    }
}

impl std::fmt::Display for Imm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Imm::Lit(i) => write!(f, "{i}"),
            Imm::Lbl(l) => write!(f, "{l}"),
        }
    }
}

impl std::fmt::Display for Operand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Operand::Imm(i) => write!(f, "${i}"),
            Operand::Reg(r) => write!(f, "{r}"),
            Operand::Ind1(i) => write!(f, "{i}"),
            Operand::Ind2(r) => write!(f, "({})", r),
            Operand::Ind3(i, r) => write!(f, "{i}({r})"),
        }
    }
}

pub fn string_of_byte_operand(op: &Operand) -> String {
    match op {
        Operand::Imm(i) => format!("${i}"),
        Operand::Reg(r) => string_of_byte_reg(r),
        Operand::Ind1(i) => i.to_string(),
        Operand::Ind2(r) => format!("({})", r),
        Operand::Ind3(i, r) => format!("{i}({r})"),
    }
}

pub fn string_of_jmp_operand(op: &Operand) -> String {
    match op {
        Operand::Imm(i) => i.to_string(),
        Operand::Reg(r) => format!("*{r}"),
        Operand::Ind1(i) => format!("*{i}"),
        Operand::Ind2(r) => format!("*({r})"),
        Operand::Ind3(i, r) => format!("{i}({r})"),
    }
}

impl std::fmt::Display for Cnd {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Cnd::Eq => write!(f, "e"),
            Cnd::Neq => write!(f, "ne"),
            Cnd::Gt => write!(f, "g"),
            Cnd::Ge => write!(f, "ge"),
            Cnd::Lt => write!(f, "l"),
            Cnd::Le => write!(f, "le"),
        }
    }
}

impl std::fmt::Display for Opcode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Opcode::Movq => write!(f, "movq"),
            Opcode::Pushq => write!(f, "pushq"),
            Opcode::Popq => write!(f, "popq"),
            Opcode::Leaq => write!(f, "leaq"),
            Opcode::Incq => write!(f, "incq"),
            Opcode::Decq => write!(f, "decq"),
            Opcode::Negq => write!(f, "negq"),
            Opcode::Notq => write!(f, "notq"),
            Opcode::Addq => write!(f, "addq"),
            Opcode::Subq => write!(f, "subq"),
            Opcode::Imulq => write!(f, "imulq"),
            Opcode::Xorq => write!(f, "xorq"),
            Opcode::Orq => write!(f, "orq"),
            Opcode::Andq => write!(f, "andq"),
            Opcode::Shlq => write!(f, "shlq"),
            Opcode::Sarq => write!(f, "sarq"),
            Opcode::Shrq => write!(f, "shrq"),
            Opcode::Jmp => write!(f, "jmp"),
            Opcode::J(c) => write!(f, "j{c}"),
            Opcode::Cmpq => write!(f, "cmpq"),
            Opcode::Set(c) => write!(f, "set{c}"),
            Opcode::Callq => write!(f, "callq"),
            Opcode::Retq => write!(f, "retq"),
        }
    }
}

fn map_join<T, F: Fn(&T) -> String>(vector: &[T], fun: F, placeholder: &str) -> String {
    vector
        .iter()
        .map(fun)
        .collect::<Vec<String>>()
        .join(placeholder)
}

impl std::fmt::Display for Ins {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.0 {
            Opcode::Shlq | Opcode::Sarq | Opcode::Shrq => match self.1[0] {
                Operand::Imm(_) => write!(
                    f,
                    "\t{}\t{}",
                    self.0,
                    map_join(&self.1, |x| x.to_string(), ",")
                ),
                Operand::Reg(Reg::Rcx) => write!(
                    f,
                    "\t{}\t%cl, {}",
                    self.0,
                    map_join(&self.1[1..self.1.len()], |x| x.to_string(), ",")
                ),
                _ => panic!(
                    "shift instruction has invalid operands: {}\n",
                    map_join(&self.1, |x| x.to_string(), ", ")
                ),
            },
            _ => write!(
                f,
                "\t{}\t{}",
                self.0,
                map_join(
                    &self.1,
                    |x| match self.0 {
                        Opcode::J(_) | Opcode::Jmp | Opcode::Callq => string_of_jmp_operand(x),
                        Opcode::Set(_) => string_of_byte_operand(x),
                        _ => x.to_string(),
                    },
                    ", "
                )
            ),
        }
    }
}

impl std::fmt::Display for Data {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Data::Asciz(s) => write!(f, "\t.asciz\t\"{s}\""),
            Data::Quad(i) => write!(f, "\t.quad\t{i}"),
        }
    }
}

impl std::fmt::Display for Asm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Asm::Text(is) => {
                write!(
                    f,
                    "\t.text\n{}",
                    is.iter()
                        .map(|x| x.to_string())
                        .collect::<Vec<String>>()
                        .join("\n")
                )
            }
            Asm::Data(ds) => {
                write!(
                    f,
                    "\t.data\n{}",
                    ds.iter()
                        .map(|x| x.to_string())
                        .collect::<Vec<String>>()
                        .join("\n")
                )
            }
        }
    }
}

impl std::fmt::Display for Elem {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let (sec, body) = match &self.asm {
            Asm::Text(is) => (
                "\t.text\n",
                is.iter()
                    .map(|x| x.to_string())
                    .collect::<Vec<String>>()
                    .join("\n"),
            ),
            Asm::Data(ds) => (
                "\t.data\n",
                ds.iter()
                    .map(|x| x.to_string())
                    .collect::<Vec<String>>()
                    .join("\n"),
            ),
        };
        let glb = if self.global {
            format!("\t.globl\t{}\n", &self.lbl)
        } else {
            String::from("")
        };
        write!(f, "{}{}{}:\n{}", sec, glb, &self.lbl, body)
    }
}

impl std::fmt::Display for Prog {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let res = self
            .0
            .iter()
            .map(|x| x.to_string())
            .collect::<Vec<String>>()
            .join("\n");
        write!(f, "{}", res)
    }
}

pub mod helpers {

    #[macro_export]
    macro_rules! lit {
        ($x:expr) => {
            x86::Operand::Imm(x86::Imm::Lit($x))
        };
        ($x:stmt) => {
            x86::Operand::Imm(x86::Imm::Lit($x))
        };
    }

    #[macro_export]
    macro_rules! reg {
        ($x:ident) => {
            x86::Operand::Reg(x86::Reg::$x)
        };
    }

    #[macro_export]
    macro_rules! lbl_imm {
        ($lbl:ident) => {
            x86::Imm::Lbl(stringify!($lbl).to_string())
        };
        ($lbl:expr) => {
            x86::Imm::Lbl($lbl)
        };
    }

    #[macro_export]
    macro_rules! lbl {
        ($lbl:ident) => {
            x86::Operand::Imm(x86::Imm::Lbl(stringify!($lbl).to_string()))
        };
        ($lbl:expr) => {
            x86::Operand::Imm(x86::Imm::Lbl($lbl))
        };
    }

    #[macro_export]
    macro_rules! ind {
        ($x:expr) => {
            x86::Operand::Ind1($x)
        };
    }

    #[macro_export]
    macro_rules! ind_reg {
        ($x:ident) => {
            x86::Operand::Ind2(x86::Reg::$x)
        };
    }

    #[macro_export]
    macro_rules! quad {
        ($x:expr) => {
            x86::Data::Quad(x86::Imm::Lit($x))
        };
    }

    #[macro_export]
    macro_rules! asciz {
        ($x:expr) => {
            x86::Data::Asciz($x.to_string())
        };
    }

    #[macro_export]
    macro_rules! ins {
        ($op:ident, $cnd:ident) => {
            x86::Ins(x86::Opcode::$op(x86::Cnd::$cnd), vec![])
        };

        ($op:ident, $cnd:ident, $($args:expr),*) => {
            x86::Ins(x86::Opcode::$op(x86::Cnd::$cnd), vec![$($args,)*])
        };

        ($op:ident, $($args:expr),*) => {
            x86::Ins(x86::Opcode::$op, vec![$($args,)*])
        };
        ($op:expr, $($args:expr),*) => {
            x86::Ins($op, vec![$($args,)*])
        };
        ($op:ident) => {
            x86::Ins(x86::Opcode::$op, vec![])
        };
        ($op:stmt) => {
            x86::Ins($op, vec![])
        };
    }

    #[macro_export]
    macro_rules! stack_offset {
        ($x:expr) => {
            x86::Operand::Ind3(x86::Imm::Lit($x), x86::Reg::Rsp)
        };
    }

    #[macro_export]
    macro_rules! prog {
        ($($args:expr),*) => {
            x86::Prog(vec![$($args,)*])
        };
        () => {
            x86::Prog(vec![])
        };
    }

    #[macro_export]
    macro_rules! text {
        ($name:ident, $($args:expr),*) => {
            x86::Elem::new(stringify!($name).to_string(), false, x86::Asm::Text(vec![$($args,)*]))
        };
        ($name:expr, $arg:expr) => {
            x86::Elem::new($name, false, x86::Asm::Text(vec![$($args,)*]))
            // match arg {
            //     => x86::Elem::new($name, false, x86::Asm::Text(vec![$($args,)*])),
            //     _ => x86::Elem::new($name, false, x86::Asm::Text(vec![$($args,)*])),
            // }
        };
        ($name:expr, $($args:expr),*) => {
            x86::Elem::new($name, false, x86::Asm::Text(vec![$($args,)*]))
        };
    }

    #[macro_export]
    macro_rules! data {
        ($name:ident, $($args:expr),*) => {
            x86::Elem::new(stringify!($name).to_string(), true, x86::Asm::Data(vec![$($args,)*]))
        };
        ($name:expr, $($args:expr),*) => {
            x86::Elem::new($name, true, x86::Asm::Data(vec![$($args,)*]))
        };
    }

    #[macro_export]
    macro_rules! gtext {
        ($name:ident, $($args:expr),*) => {
            x86::Elem::new(stringify!($name).to_string(), true, x86::Asm::Text(vec![$($args,)*]))
        };
        ($name:expr, $($args:expr),*) => {
            x86::Elem::new($name, true, x86::Asm::Text(vec![$($args,)*]))
        };
    }
}
