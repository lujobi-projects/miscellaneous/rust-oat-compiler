pub mod simulator;
mod utils;
pub use crate::simulator::*;

// X86lite language representation.

// assembler syntax ---------------------------------------------------------
// Findings needs better differenciation between pointer imm type and number imm

// Labels for code blocks and global data.
pub type Lbl = String;

pub type Quad = i64;

// Immediate operands
#[derive(Clone, Debug, PartialEq)]
pub enum Imm {
    Lit(Quad),
    Lbl(Lbl),
}
/* Registers:
    instruction pointer: rip
    arguments: rdi, rsi, rdx, rcx, r09, r08
    callee-save: rbx, rbp, r12-r15
*/
#[derive(Clone, Debug, PartialEq)]
pub enum Reg {
    Rip,
    Rax,
    Rbx,
    Rcx,
    Rdx,
    Rsi,
    Rdi,
    Rbp,
    Rsp,
    R08,
    R09,
    R10,
    R11,
    R12,
    R13,
    R14,
    R15,
}
#[derive(Clone, Debug, PartialEq)]
pub enum Operand {
    Imm(Imm),       // immediate
    Reg(Reg),       // register
    Ind1(Imm),      // indirect: displacement
    Ind2(Reg),      // indirect: (%reg)
    Ind3(Imm, Reg), // indirect: displacement(%reg)
}

// Condition Codes
#[derive(Clone, Debug, PartialEq, Copy)]
pub enum Cnd {
    Eq,
    Neq,
    Gt,
    Ge,
    Lt,
    Le,
}

#[derive(Clone, Debug, Copy, PartialEq)]
pub enum Opcode {
    Movq,
    Pushq,
    Popq,
    Leaq,
    Incq,
    Decq,
    Negq,
    Notq,
    Addq,
    Subq,
    Imulq,
    Xorq,
    Orq,
    Andq,
    Shlq,
    Sarq,
    Shrq,
    Jmp,
    J(Cnd),
    Cmpq,
    Set(Cnd),
    Callq,
    Retq,
}

/* An instruction is an Opcode plus its operands.
    Note that arity and other constraints about the operands
    are not checked.
*/
pub type Args = Vec<Operand>;

#[derive(Clone, Debug, PartialEq)]
pub struct Ins(pub Opcode, pub Args);

#[derive(Clone, Debug)]
pub enum Data {
    Asciz(String),
    Quad(Imm),
}

#[derive(Clone, Debug)]
pub enum Asm {
    Text(Vec<Ins>),
    Data(Vec<Data>),
}

// labeled blocks of data or code
#[derive(Clone, Debug)]
pub struct Elem {
    pub lbl: Lbl,
    pub global: bool,
    pub asm: Asm,
}
impl Elem {
    pub fn new(lbl: Lbl, global: bool, asm: Asm) -> Elem {
        Elem { lbl, global, asm }
    }
}

pub struct Prog(pub Vec<Elem>);
